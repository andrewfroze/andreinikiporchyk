package com.epam.tat.module4;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
public class CalculatorTestWithTwoParametersDouble extends BaseTest {
    private double a;
    private double b;

    public CalculatorTestWithTwoParametersDouble(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Test
    public void testSum() {
        double sum = calculator.sum(a, b);
        double expectedResult = a + b;
        Assert.assertEquals(sum, expectedResult, 0.000001);
    }

    @Test
    public void testSub() {

        double sub = calculator.sub(a, b);
        double expectedResult = a - b;
        Assert.assertEquals(sub, expectedResult, 0.000001);
    }

    @Test
    public void testMult() {
        double mult = calculator.mult(a, b);
        double expectedResult = a * b;
        Assert.assertEquals(mult, expectedResult, 0.000001);
    }

    @Test
    public void testDiv() {
        double div = calculator.div(a, b);
        double expectedResult = a / b;
        Assert.assertEquals(div, expectedResult);
    }

    @Test
    public void testPow() {
        double pow = calculator.pow(a, b);
        double expectedResult = Math.pow(a, b);
        Assert.assertEquals(pow, expectedResult, 0.000001);
    }

}
