package com.epam.tat.module4;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

public class FactoryCalculatorTestDouble {

    @DataProvider(name = "Data for two parameters operators double type")
    public Object[][] dataForTwoParametersOperatorsDouble() {
        return new Object[][]{
                {2.1, 3.2},
                {2.2, -3.5},
                {3.1, 0.0},
                {5.1, 2.5},
        };
    }

    @Factory(dataProvider = "Data for two parameters operators double type")
    public Object[] createTestForTwoDoubleParameters(double ad, double bd) {
        return new Object[]{new CalculatorTestWithTwoParametersDouble(ad, bd)};
    }
}
