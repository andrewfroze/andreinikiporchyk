package com.epam.tat.module4;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
public class CalculatorTestPositiveNegative extends BaseTest {

    @DataProvider(name = "Data for positive/negative definition")
    public Object[][] dataForOneParametersOperators() {
        return new Object[][]{
                {1},
                {-1},
                {0}
        };
    }

    @Test(dataProvider = "Data for positive/negative definition")
    public void testIsPositive(long x) {
        boolean expectedResult = x > 0;
        Assert.assertEquals(calculator.isPositive(x), expectedResult);
    }

    @Test(dataProvider = "Data for positive/negative definition")
    public void testIsNegative(long x) {
        boolean expectedResult = x < 0;
        Assert.assertEquals(calculator.isNegative(x), expectedResult);
    }
}
