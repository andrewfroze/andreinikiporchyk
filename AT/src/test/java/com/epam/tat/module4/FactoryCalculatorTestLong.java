package com.epam.tat.module4;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

public class FactoryCalculatorTestLong {
    @DataProvider(name = "Data for two parameters operators Long type")
    public Object[][] dataForTwoParametersOperatorsLong() {
        return new Object[][]{
                {2L, 3L},
                {2L, -3L},
                {3L, 0L},
                {5L, 2L},
        };
    }

    @Factory(dataProvider = "Data for two parameters operators Long type")
    public Object[] createTestForTwoLongParameters(Long al, Long bl) {
        return new Object[]{new CalculatorTestWithTwoParametersLong(al, bl)};
    }
}
