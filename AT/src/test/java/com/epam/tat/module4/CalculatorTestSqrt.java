package com.epam.tat.module4;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
public class CalculatorTestSqrt extends BaseTest {

    @DataProvider(name = "Data for sqrt test")
    public Object[][] dataForTrigonometricOperators() {
        return new Object[][]{
                {5},
                {-5},
                {25},
                {0},
        };
    }

    @Test(dataProvider = "Data for sqrt test")
    public void testSqrt(double x) {
        double sqrtX = calculator.sqrt(x);
        double expectedResult = Math.sqrt(x);
        Assert.assertEquals(sqrtX, expectedResult, 0.000001);
    }
}
