package com.epam.tat.module4;

import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

public class TestRunner {

    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        testNG.addListener(new TestListener());
        List<String> files = Arrays.asList(
                "./src/test/resources/testSimpleOperators.xml",
                "./src/test/resources/testTrigonometric.xml",
                "./src/test/resources/testPositiveNegative.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}
