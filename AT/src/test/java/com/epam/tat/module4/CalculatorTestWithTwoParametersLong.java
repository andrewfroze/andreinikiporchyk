package com.epam.tat.module4;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
public class CalculatorTestWithTwoParametersLong extends BaseTest {
    private Long a;
    private Long b;

    public CalculatorTestWithTwoParametersLong(Long a, Long b) {
        this.a = a;
        this.b = b;
    }

    @Test
    public void testSum() {
        Long sum = calculator.sum(a, b);
        Long expectedResult = a + b;
        Assert.assertEquals(sum, expectedResult);
    }

    @Test
    public void testSub() {
        Long sub = calculator.sub(a, b);
        Long expectedResult = a - b;
        Assert.assertEquals(sub, expectedResult);
    }

    @Test
    public void testMult() {
        Long mult = calculator.mult(a, b);
        Long expectedResult = a * b;
        Assert.assertEquals(mult, expectedResult);
    }

    @Test
    public void testDiv() {
        if (b != 0) {
            Long div = calculator.div(a, b);
            Long expectedResult = a / b;
            Assert.assertEquals(div, expectedResult);
        } else {
            Assert.assertThrows(NumberFormatException.class, () -> {
                throw new NumberFormatException("divide by zero");
            });
        }
    }
}
