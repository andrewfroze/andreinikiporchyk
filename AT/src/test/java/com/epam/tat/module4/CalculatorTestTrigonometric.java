package com.epam.tat.module4;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestListener.class})
public class CalculatorTestTrigonometric extends BaseTest {

    @DataProvider(name = "Data for trigonometric tests")
    public Object[][] dataForTrigonometricOperators() {
        return new Object[][]{
                {1},
                {-1},
                {0},
                {3.14},
                {1.57},
                {-1.57},
        };
    }

    @Test(dataProvider = "Data for trigonometric tests", priority = 1)
    public void testSin(double x) {
        double sinX = calculator.sin(x);
        double expectedResult = Math.sin(x);
        Assert.assertEquals(sinX, expectedResult, 0.000001);
    }

    @Test(dataProvider = "Data for trigonometric tests", priority = 2)
    public void testCos(double x) {
        double cosX = calculator.cos(x);
        double expectedResult = Math.cos(x);
        Assert.assertEquals(cosX, expectedResult, 0.000001);
    }

    @Test(dataProvider = "Data for trigonometric tests", priority = 3)
    public void testTg(double x) {
        double tanX = calculator.tg(x);
        double expectedResult = Math.tan(x);
        Assert.assertEquals(tanX, expectedResult, 0.000001);
    }

    @Test(dataProvider = "Data for trigonometric tests", priority = 4)
    public void testCtg(double x) {
        double tanX = calculator.ctg(x);
        double expectedResult = 1 / Math.tan(x);
        Assert.assertEquals(tanX, expectedResult, 0.000001);
    }
}
