package com.epam.tat.module4;

import org.testng.annotations.BeforeClass;

public class BaseTest {
    protected Calculator calculator;

    @BeforeClass
    public void createCalculator() {
        calculator = new Calculator();
    }
}
