package com.training.an.AThw_4.tests;

import com.training.an.AThw_4.constants.RadioLocation;
import com.training.an.AThw_4.constants.Speed;
import com.training.an.AThw_4.po.TooltipPage;
import com.training.an.AThw_4.po.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestActions extends TestConfiguration {
    private static final String firstTooltipExpected = "That's what this widget is";
    private static final String secondTooltipExpected = "ThemeRoller: jQuery UI's theme builder application";
    private static final String thirdTooltipExpected = "We ask for your age only for statistical purposes.";

    @Test
    public void testDragAndDrop() {
        DroppablePage droppablePage = new MainPage(driver).goToDroppablePage().dragAndDrop();
        Assert.assertTrue(droppablePage.getDropStatus());
    }

    @Test
    public void testRadioButton() {
        CheckboxradioPage checkboxradioPage = new MainPage(driver).goToCheckboxradioPage()
                .selectALocation(RadioLocation.NEW_YORK);
        Assert.assertEquals(checkboxradioPage.getSelectedLocation(), RadioLocation.NEW_YORK);
    }

    @Test
    public void testCheckbox() {
        CheckboxradioPage checkboxradioPage = new MainPage(driver).goToCheckboxradioPage()
                .selectHotelRatings(2);
        Assert.assertEquals(checkboxradioPage.getNumberOfSelectedHotelRates(), 1);
        Assert.assertEquals(checkboxradioPage.getSelectedHotelRateOnPosition(1), "2 Star");
    }

    @Test
    public void testSelectSpeed() {
        SelectMenuPage selectMenuPage = new MainPage(driver).goToSelectMenuPage()
                .selectSpeed(Speed.FAST);
        Assert.assertEquals(selectMenuPage.getSelectedSpeed(), Speed.FAST);
    }

    @Test
    public void testSelectNumber() {
        SelectMenuPage selectMenuPage = new MainPage(driver).goToSelectMenuPage()
                .selectNumber(3);
        Assert.assertEquals(selectMenuPage.getSelectedNumber(), 3);
    }

    @Test
    public void testTooltips() {
        TooltipPage tooltipPage = new MainPage(driver).goToTooltipPage().moveToFirstLink();
        String firstTooltip = tooltipPage.getLastInLog();
        tooltipPage = tooltipPage.moveToSecondLink();
        String secondTooltip = tooltipPage.getLastInLog();
        tooltipPage = tooltipPage.moveToAgeField();
        String thirdTooltip = tooltipPage.getLastInLog();

        Assert.assertEquals(firstTooltip, firstTooltipExpected);
        Assert.assertEquals(secondTooltip, secondTooltipExpected);
        Assert.assertEquals(thirdTooltip, thirdTooltipExpected);
    }

    @Test
    public void testSortableList() {
        SortablePage sortablePage = new MainPage(driver).goToSortablePage()
                .moveItemToPosition(7, 1)
                .moveItemToPosition(5, 3)
                .moveItemToPosition(3, 7);

        Assert.assertEquals(sortablePage.getItemOnPosition(1), "Item 7");
        Assert.assertEquals(sortablePage.getItemOnPosition(2), "Item 1");
        Assert.assertEquals(sortablePage.getItemOnPosition(3), "Item 5");
        Assert.assertEquals(sortablePage.getItemOnPosition(4), "Item 2");
        Assert.assertEquals(sortablePage.getItemOnPosition(5), "Item 4");
        Assert.assertEquals(sortablePage.getItemOnPosition(6), "Item 6");
        Assert.assertEquals(sortablePage.getItemOnPosition(7), "Item 3");
    }

    @Test
    public void testDatapicker() {
        DatepickerPage datepickerPage = new MainPage(driver).goToDatepickerPage()
                .selectDate(12, 2, 2018);

        Assert.assertEquals(datepickerPage.getSelectedDay(), 2);
        Assert.assertEquals(datepickerPage.getSelectedMonth(), 12);
        Assert.assertEquals(datepickerPage.getSelectedYear(), 2018);
    }
}
