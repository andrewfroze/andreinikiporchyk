package com.training.an.AThw_4.po;

import com.training.an.AThw_4.constants.RadioLocation;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class CheckboxradioPage extends MainPage {
    private static final By demoIframe = By.cssSelector("iframe.demo-frame");
    private static final By radioButtons = By.xpath("//label[contains(@for,'radio')]");
    private static final By activeRadioButton = By.xpath("//label[contains(@for,'radio') and contains(@class,'ui-state-active')]");
    private static final By checkboxHotelRates = By.xpath("//legend[contains(text(),'Hotel')]/../label[contains(@for,'checkbox')]");
    private static final By selectedCheckboxHotelRates = By.xpath("//legend[contains(text(),'Hotel')]/../label[contains(@for,'checkbox') and contains(@class,'ui-state-active')]");

    public CheckboxradioPage(WebDriver driver) {
        super(driver);
    }

    public CheckboxradioPage selectALocation(RadioLocation city) {
        driver.switchTo().frame(driver.findElement(demoIframe));
        Actions actions = new Actions(driver);
        List<WebElement> radioButtonsList = driver.findElements(radioButtons);
        for (WebElement element : radioButtonsList) {
            if (element.getText().toUpperCase().replaceAll(" ", "_").equals(city.toString())) {
                actions.click(element).build().perform();
            }
        }
        driver.switchTo().defaultContent();
        return new CheckboxradioPage(driver);
    }

    public RadioLocation getSelectedLocation() {
        RadioLocation SelectedLocation = null;
        driver.switchTo().frame(driver.findElement(demoIframe));
        for (RadioLocation location : RadioLocation.values()) {
            if (driver.findElement(activeRadioButton).getText().toUpperCase().replaceAll(" ", "_").equals(location.toString())) {
                SelectedLocation = location;
            }
        }
        driver.switchTo().defaultContent();
        return SelectedLocation;
    }

    public CheckboxradioPage selectHotelRatings(int rate) {
        if (rate > 1 && rate < 6) {
            driver.switchTo().frame(driver.findElement(demoIframe));
            Actions actions = new Actions(driver);
            List<WebElement> hotelRatesCheckboxes = driver.findElements(checkboxHotelRates);
            for (WebElement element : hotelRatesCheckboxes) {
                if (element.getText().equals(rate + " Star")) {
                    actions.click(element).build().perform();
                }
            }
            driver.switchTo().defaultContent();
        } else {
            System.out.println("There isn't hotel rate " + rate + " Stars");
        }
        return new CheckboxradioPage(driver);
    }

    public int getNumberOfSelectedHotelRates() {
        driver.switchTo().frame(driver.findElement(demoIframe));
        List<WebElement> selectedHotelRates = driver.findElements(selectedCheckboxHotelRates);
        int result = selectedHotelRates.size();
        driver.switchTo().defaultContent();
        return result;
    }

    public String getSelectedHotelRateOnPosition(int position) {
        driver.switchTo().frame(driver.findElement(demoIframe));
        List<WebElement> selectedHotelRates = driver.findElements(selectedCheckboxHotelRates);
        String result = null;
        if (position <= selectedHotelRates.size()) {
            result = selectedHotelRates.get(position - 1).getText();
        } else {
            System.out.println("There isn't " + position + " position. Only " + selectedHotelRates.size() + " element(s) is(are) selected.");
        }
        driver.switchTo().defaultContent();
        return result;
    }
}
