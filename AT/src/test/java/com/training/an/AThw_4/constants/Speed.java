package com.training.an.AThw_4.constants;

public enum Speed {
    SLOWER, SLOW, MEDIUM, FAST, FASTER
}
