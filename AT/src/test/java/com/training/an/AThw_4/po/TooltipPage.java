package com.training.an.AThw_4.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class TooltipPage extends MainPage {
    private static final By demoIframe = By.cssSelector("iframe.demo-frame");
    private static final By tooltipContent = By.xpath("//div[@role='tooltip']/div[@class='ui-tooltip-content']");
    private static final By firstLinkWithTooltip = By.xpath("//a[contains(text(),'Tooltips')]");
    private static final By secondLinkWithTooltip = By.xpath("//a[contains(text(),'ThemeRoller')]");
    private static final By ageFieldWithTooltip = By.id("age");
    private static final By lastInLog = By.xpath("//div[@role='log']/div[last()]");

    public TooltipPage(WebDriver driver) {
        super(driver);
    }

    public String getTooltipContent() {
        driver.switchTo().frame(driver.findElement(demoIframe));
        String content = driver.findElement(tooltipContent).getText();
        driver.switchTo().defaultContent();
        return content;
    }

    public String getLastInLog() {
        driver.switchTo().frame(driver.findElement(demoIframe));
        String text = driver.findElement(lastInLog).getText();
        driver.switchTo().defaultContent();
        return text;
    }

    public TooltipPage moveToFirstLink() {
        driver.switchTo().frame(driver.findElement(demoIframe));
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(firstLinkWithTooltip)).build().perform();
        driver.switchTo().defaultContent();
        return new TooltipPage(driver);
    }

    public TooltipPage moveToSecondLink() {
        driver.switchTo().frame(driver.findElement(demoIframe));
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(secondLinkWithTooltip)).build().perform();
        driver.switchTo().defaultContent();
        return new TooltipPage(driver);
    }

    public TooltipPage moveToAgeField() {
        driver.switchTo().frame(driver.findElement(demoIframe));
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(ageFieldWithTooltip)).build().perform();
        driver.switchTo().defaultContent();
        return new TooltipPage(driver);
    }
}
