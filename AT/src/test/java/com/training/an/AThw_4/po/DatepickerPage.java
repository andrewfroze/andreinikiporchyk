package com.training.an.AThw_4.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class DatepickerPage extends MainPage {
    private static final By demoIframe = By.cssSelector("iframe.demo-frame");
    private static final By firstDayOfMonth = By.xpath("//tr[1]/td[@data-handler='selectDay'][1]");
    private static final By previousMonth = By.xpath("//a[@title='Prev']");
    private static final By nextMonth = By.xpath("//a[@title='Next']");
    private static final By dateInput = By.id("datepicker");
    private static final By daysOfMonth = By.xpath("//tr/td[@data-handler='selectDay']/a");
    private static final By selectedDay = By.xpath("//tr/td[@data-handler='selectDay']/a[contains(@class,'ui-state-active')]");
    private static final By selectedCell = By.xpath("//tr/td[@data-handler='selectDay']/a[contains(@class,'ui-state-active')]/..");

    public DatepickerPage(WebDriver driver) {
        super(driver);
    }

    public int getSelectedDay() {
        driver.switchTo().frame(driver.findElement(demoIframe));
        Actions actions = new Actions(driver);
        actions.click(driver.findElement(dateInput)).build().perform();
        int day = Integer.parseInt(driver.findElement(selectedDay).getText());
        driver.switchTo().defaultContent();
        return day;
    }

    public int getSelectedMonth() {
        driver.switchTo().frame(driver.findElement(demoIframe));
        Actions actions = new Actions(driver);
        actions.click(driver.findElement(dateInput)).build().perform();
        int month = Integer.parseInt(driver.findElement(selectedCell).getAttribute("data-month")) + 1;
        driver.switchTo().defaultContent();
        return month;
    }

    public int getSelectedYear() {
        driver.switchTo().frame(driver.findElement(demoIframe));
        Actions actions = new Actions(driver);
        actions.click(driver.findElement(dateInput)).build().perform();
        int year = Integer.parseInt(driver.findElement(selectedCell).getAttribute("data-year"));
        driver.switchTo().defaultContent();
        return year;
    }

    public DatepickerPage selectDate(int month, int day, int year) {
        month--;
        driver.switchTo().frame(driver.findElement(demoIframe));
        Actions actions = new Actions(driver);
        actions.click(driver.findElement(dateInput)).build().perform();
        int currentMonth = Integer.parseInt(driver.findElement(firstDayOfMonth).getAttribute("data-month"));
        int currentYear = Integer.parseInt(driver.findElement(firstDayOfMonth).getAttribute("data-year"));
        By searcherOfMonthAndYear = null;
        if ((year < currentYear) || (currentYear == year && month < currentMonth)) {
            searcherOfMonthAndYear = previousMonth;
        } else if ((year > currentYear) || (currentYear == year && month > currentMonth)) {
            searcherOfMonthAndYear = nextMonth;
        }
        while (!(currentYear == year && currentMonth == month)) {
            actions.click(driver.findElement(searcherOfMonthAndYear)).build().perform();
            currentMonth = Integer.parseInt(driver.findElement(firstDayOfMonth).getAttribute("data-month"));
            currentYear = Integer.parseInt(driver.findElement(firstDayOfMonth).getAttribute("data-year"));
        }
        List<WebElement> days = driver.findElements(daysOfMonth);
        for (WebElement element : days) {
            if (Integer.parseInt(element.getText()) == day) {
                actions.click(element).build().perform();
                break;
            }
        }
        driver.switchTo().defaultContent();
        return new DatepickerPage(driver);
    }
}
