package com.training.an.AThw_4.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class DroppablePage extends MainPage {
    private static final By demoIframe = By.cssSelector("iframe.demo-frame");
    private static final By draggableSquare = By.id("draggable");
    private static final By targetSquare = By.id("droppable");
    private static final By dropStatus = By.xpath("//div[@id='droppable']/p");

    public DroppablePage(WebDriver driver) {
        super(driver);
    }

    public boolean getDropStatus() {
        driver.switchTo().frame(driver.findElement(demoIframe));
        if (driver.findElement(dropStatus).getText().equals("Dropped!")) {
            driver.switchTo().defaultContent();
            return true;
        } else {
            driver.switchTo().defaultContent();
            return false;
        }
    }

    public DroppablePage dragAndDrop() {
        driver.switchTo().frame(driver.findElement(demoIframe));
        Actions actions = new Actions(driver);
        actions.dragAndDrop(driver.findElement(draggableSquare), driver.findElement(targetSquare))
                .build()
                .perform();
        driver.switchTo().defaultContent();
        return new DroppablePage(driver);
    }
}
