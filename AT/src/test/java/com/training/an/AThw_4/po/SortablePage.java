package com.training.an.AThw_4.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class SortablePage extends MainPage {
    private static final By demoIframe = By.cssSelector("iframe.demo-frame");
    private static final By sortableListItems = By.cssSelector("ul#sortable>li");

    public SortablePage(WebDriver driver) {
        super(driver);
    }

    public String getItemOnPosition(int position) {
        String item = null;
        if (position > 0 && position < 8) {
            driver.switchTo().frame(driver.findElement(demoIframe));
            List<WebElement> listOfItems = driver.findElements(sortableListItems);
            item = listOfItems.get(position - 1).getText();
            driver.switchTo().defaultContent();
        } else {
            System.out.println("There is only 7 items");
        }
        return item;
    }

    public SortablePage moveItemToPosition(int item, int position) {
        driver.switchTo().frame(driver.findElement(demoIframe));
        List<WebElement> listOfItems = driver.findElements(sortableListItems);
        Actions actions = new Actions(driver);
        for (WebElement element : listOfItems) {
            if (!element.equals(listOfItems.get(position - 1)) && Integer.parseInt(element.getText().replaceAll("\\D+","")) == item) {
                int yOffset = listOfItems.get(position - 1).getLocation().getY() - element.getLocation().getY();
                if (yOffset > 0) {
                    yOffset += 5;
                } else {
                    yOffset -= 5;
                }
                actions.dragAndDropBy(element, 0, yOffset).build().perform();
                break;
            }
        }
        driver.switchTo().defaultContent();
        return new SortablePage(driver);
    }
}
