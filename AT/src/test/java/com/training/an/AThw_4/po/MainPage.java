package com.training.an.AThw_4.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends Page {
    protected static final By droppableLink = By.xpath(".//*[@id='sidebar']//a[contains(@href,'droppable')]");
    protected static final By checkboxradioLink = By.xpath(".//*[@id='sidebar']//a[contains(@href,'checkboxradio')]");
    protected static final By selectmenuLink = By.xpath(".//*[@id='sidebar']//a[contains(@href,'selectmenu')]");
    protected static final By tooltipLink = By.xpath(".//*[@id='sidebar']//a[contains(@href,'tooltip')]");
    protected static final By sortableLink = By.xpath(".//*[@id='sidebar']//a[contains(@href,'sortable')]");
    protected static final By datepickerLink = By.xpath(".//*[@id='sidebar']//a[contains(@href,'datepicker')]");

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public CheckboxradioPage goToCheckboxradioPage() {
        driver.findElement(checkboxradioLink).click();
        return new CheckboxradioPage(driver);
    }

    public DroppablePage goToDroppablePage() {
        driver.findElement(droppableLink).click();
        return new DroppablePage(driver);
    }

    public SelectMenuPage goToSelectMenuPage() {
        driver.findElement(selectmenuLink).click();
        return new SelectMenuPage(driver);
    }

    public TooltipPage goToTooltipPage() {
        driver.findElement(tooltipLink).click();
        return new TooltipPage(driver);
    }

    public SortablePage goToSortablePage() {
        driver.findElement(sortableLink).click();
        return new SortablePage(driver);
    }

    public DatepickerPage goToDatepickerPage() {
        driver.findElement(datepickerLink).click();
        return new DatepickerPage(driver);
    }
}
