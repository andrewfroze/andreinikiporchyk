package com.training.an.AThw_4.po;

import com.training.an.AThw_4.constants.Speed;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SelectMenuPage extends MainPage {
    private static final By demoIframe = By.cssSelector("iframe.demo-frame");
    private static final By selectSpeedButton = By.id("speed-button");
    private static final By speedsInMenu = By.xpath("//li[@class='ui-menu-item']/div");
    private static final By selectedSpeed = By.cssSelector("span#speed-button>span.ui-selectmenu-text");
    private static final By selectNumberButton = By.id("number-button");
    private static final By numbersInMenu = By.cssSelector("ul#number-menu>li>div");
    private static final By selectedNumber = By.cssSelector("span#number-button>span.ui-selectmenu-text");


    public SelectMenuPage(WebDriver driver) {
        super(driver);
    }

    public Speed getSelectedSpeed() {
        driver.switchTo().frame(driver.findElement(demoIframe));
        Speed result = null;
        for (Speed speed : Speed.values()) {
            if (speed.toString().equals(driver.findElement(selectedSpeed).getText().toUpperCase())) {
                result = speed;
            }
        }
        driver.switchTo().defaultContent();
        return result;
    }

    public int getSelectedNumber() {
        driver.switchTo().frame(driver.findElement(demoIframe));
        int result = Integer.parseInt(driver.findElement(selectedNumber).getText());
        driver.switchTo().defaultContent();
        return result;
    }

    public SelectMenuPage selectSpeed(Speed speed) {
        driver.switchTo().frame(driver.findElement(demoIframe));
        driver.findElement(selectSpeedButton).click();
        List<WebElement> speedsForChoose = driver.findElements(speedsInMenu);
        for (WebElement element : speedsForChoose) {
            if (element.getText().toUpperCase().equals(speed.toString())) {
                System.out.println(element.getText());
                element.click();
                break;
            }
        }
        driver.switchTo().defaultContent();
        return new SelectMenuPage(driver);
    }

    public SelectMenuPage selectNumber(int number) {
        if (number > 0 && number < 20) {
            driver.switchTo().frame(driver.findElement(demoIframe));
            driver.findElement(selectNumberButton).click();
            List<WebElement> numbersForChoose = driver.findElements(numbersInMenu);
            for (WebElement element : numbersForChoose) {
                if (Integer.parseInt(element.getText()) == number) {
                    element.click();
                    break;
                }
            }
            driver.switchTo().defaultContent();
        } else {
            System.out.println("You can choose only numbers from 1 to 19");
        }
        return new SelectMenuPage(driver);
    }
}
