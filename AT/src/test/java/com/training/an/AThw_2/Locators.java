package com.training.an.AThw_2;

import org.openqa.selenium.By;

public class Locators {
    public static final By emailField = By.cssSelector("input[type='email']");
    public static final By buttonNext = By.xpath("//span[text()='Далее']/..");
    public static final By passwordField = By.cssSelector("input[type='password']");
    public static final By accountInformation = By.xpath("//div[@aria-label='Account Information']");
    public static final By createLetterButton = By.xpath("//div[contains(text(), 'Compose') and @role='button']");
    public static final By dialogWindow = By.cssSelector("div[role='dialog']");
    public static final By addressLine = By.xpath("//div[@aria-label='New Message']/descendant::textarea[@aria-label='To']");
    public static final By subjectLine = By.cssSelector("input[name='subjectbox']");
    public static final By messageBody = By.xpath("//div[@aria-label='Message Body']");
    public static final By saveAndClose = By.cssSelector("img[data-tooltip='Save & close']");
    public static final By drafts = By.xpath("//a[@aria-label='Drafts']");
    public static final By firstDraft = By.xpath("//tr[@class='zA yO'][1]");
    public static final By addressForCheck = By.xpath("//input[@name='to']/../span");
    public static final By subjectForCheck = By.cssSelector("input[name='subject']");
    public static final By sendButton = By.xpath("//div[@role='button' and contains(text(),'Send')]");
    public static final By emptyDrafts = By.cssSelector("td.TC");
    public static final By sentButton = By.cssSelector("a[aria-label='Sent']");
    public static final By sentLetters = By.xpath("//div[@class='Cp']//table[@cellpadding='0' and @role='grid']");
    public static final By checkLetterIsInSent = By.xpath("//table[@class='F cf zt']/tbody/tr[1]");
    public static final By accountInformationButton = By.xpath("//img[@class='gb_Da gbii']/..");
    public static final By signOutButton = By.xpath("//div[@aria-label='Account Information']//a[@target='_top']");
    public static final By logoffPage = By.xpath("//h1[@class='ahT6S ']");
}
