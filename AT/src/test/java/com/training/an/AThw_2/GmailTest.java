package com.training.an.AThw_2;

import org.testng.Assert;
import org.testng.annotations.Test;

import static com.training.an.AThw_2.Locators.*;

public class GmailTest extends ConfigurationTest {

    @Test
    public void testLogin() {
        waitForElementIsPresent(emailField);
        driver.findElement(emailField).clear();
        driver.findElement(emailField).sendKeys(user.getAddress());
        driver.findElement(buttonNext).click();
        waitForElementVisibility(passwordField);
        driver.findElement(passwordField).sendKeys(user.getPassword());
        driver.findElement(buttonNext).click();
        waitForElementIsPresent(accountInformationButton);
        waitForElementClickable(accountInformationButton);
        driver.findElement(accountInformationButton).click();
        waitForElementVisibility(accountInformation);

        Assert.assertTrue(elementIsPresent(accountInformation));
    }

    @Test
    public void testCreateNewMail() {
        waitForElementIsPresent(emailField);
        driver.findElement(emailField).clear();
        driver.findElement(emailField).sendKeys(user.getAddress());
        driver.findElement(buttonNext).click();
        waitForElementVisibility(passwordField);
        driver.findElement(passwordField).sendKeys(user.getPassword());
        driver.findElement(buttonNext).click();

        waitForElementClickable(createLetterButton);
        driver.findElement(createLetterButton).click();
        waitForElementIsPresent(dialogWindow);
        driver.findElement(addressLine).clear();
        driver.findElement(addressLine).sendKeys(testLetter.getAddress());
        driver.findElement(subjectLine).clear();
        driver.findElement(subjectLine).sendKeys(testLetter.getSubject());
        driver.findElement(messageBody).clear();
        driver.findElement(messageBody).sendKeys(testLetter.getMessage());
        driver.findElement(saveAndClose).click();
        waitForElementClickable(drafts);
        driver.findElement(drafts).click();
        waitForElementIsPresent(firstDraft);

        Assert.assertTrue(elementIsPresent(firstDraft));
    }

    @Test
    public void testDraftContent() {
        waitForElementIsPresent(emailField);
        driver.findElement(emailField).clear();
        driver.findElement(emailField).sendKeys(user.getAddress());
        driver.findElement(buttonNext).click();
        waitForElementVisibility(passwordField);
        driver.findElement(passwordField).sendKeys(user.getPassword());
        driver.findElement(buttonNext).click();

        waitForElementClickable(createLetterButton);
        driver.findElement(createLetterButton).click();
        waitForElementIsPresent(dialogWindow);
        driver.findElement(addressLine).clear();
        driver.findElement(addressLine).sendKeys(testLetter.getAddress());
        driver.findElement(subjectLine).clear();
        driver.findElement(subjectLine).sendKeys(testLetter.getSubject());
        driver.findElement(messageBody).clear();
        driver.findElement(messageBody).sendKeys(testLetter.getMessage());
        driver.findElement(saveAndClose).click();
        waitForElementClickable(drafts);
        driver.findElement(drafts).click();
        waitForElementClickable(firstDraft);
        driver.findElement(firstDraft).click();
        waitForElementIsPresent(addressForCheck);
        waitForElementIsPresent(subjectForCheck);
        waitForElementIsPresent(messageBody);

        String actualDraftAddress = driver.findElement(addressForCheck).getAttribute("email");
        String actualDraftSubject = driver.findElement(subjectForCheck).getAttribute("value");
        String actualDraftMessage = driver.findElement(messageBody).getText();
        Letter actualLetter = new Letter(actualDraftAddress, actualDraftSubject, actualDraftMessage);

        Assert.assertEquals(actualLetter, testLetter);
    }

    @Test
    public void testSendMail() {
        waitForElementIsPresent(emailField);
        driver.findElement(emailField).clear();
        driver.findElement(emailField).sendKeys(user.getAddress());
        driver.findElement(buttonNext).click();
        waitForElementVisibility(passwordField);
        driver.findElement(passwordField).sendKeys(user.getPassword());
        driver.findElement(buttonNext).click();

        waitForElementClickable(createLetterButton);
        driver.findElement(createLetterButton).click();
        waitForElementIsPresent(dialogWindow);
        driver.findElement(addressLine).clear();
        driver.findElement(addressLine).sendKeys(testLetter.getAddress());
        driver.findElement(subjectLine).clear();
        driver.findElement(subjectLine).sendKeys(testLetter.getSubject());
        driver.findElement(messageBody).clear();
        driver.findElement(messageBody).sendKeys(testLetter.getMessage());
        driver.findElement(saveAndClose).click();
        waitForElementClickable(drafts);
        driver.findElement(drafts).click();

        waitForElementClickable(firstDraft);
        String firstDraftIdBefore = driver.findElement(firstDraft).getAttribute("id");
        driver.findElement(firstDraft).click();
        waitForElementIsPresent(addressForCheck);
        waitForElementIsPresent(subjectForCheck);
        waitForElementIsPresent(messageBody);
        waitForElementClickable(sendButton);
        driver.findElement(sendButton).click();
        String firstDraftIdAfter = null;
        if (elementIsPresent(firstDraft)) {
            firstDraftIdAfter = driver.findElement(firstDraft).getAttribute("id");
            Assert.assertNotEquals(firstDraftIdAfter, firstDraftIdBefore);
        } else {
            Assert.assertTrue(elementIsPresent(emptyDrafts));
        }
    }

    @Test
    public void testMailIsInSent() {
        waitForElementIsPresent(emailField);
        driver.findElement(emailField).clear();
        driver.findElement(emailField).sendKeys(user.getAddress());
        driver.findElement(buttonNext).click();
        waitForElementVisibility(passwordField);
        driver.findElement(passwordField).sendKeys(user.getPassword());
        driver.findElement(buttonNext).click();

        waitForElementClickable(createLetterButton);
        driver.findElement(createLetterButton).click();
        waitForElementIsPresent(dialogWindow);
        driver.findElement(addressLine).clear();
        driver.findElement(addressLine).sendKeys(testLetter.getAddress());
        driver.findElement(subjectLine).clear();
        driver.findElement(subjectLine).sendKeys(testLetter.getSubject());
        driver.findElement(messageBody).clear();
        driver.findElement(messageBody).sendKeys(testLetter.getMessage());
        driver.findElement(saveAndClose).click();
        waitForElementClickable(drafts);
        driver.findElement(drafts).click();

        waitForElementClickable(firstDraft);
        driver.findElement(firstDraft).click();
        waitForElementIsPresent(addressForCheck);
        waitForElementIsPresent(subjectForCheck);
        waitForElementIsPresent(messageBody);
        waitForElementClickable(sendButton);
        driver.findElement(sendButton).click();

        waitForElementClickable(sentButton);
        driver.findElement(sentButton).click();
        waitForElementIsPresent(sentLetters);

        Assert.assertTrue(elementIsPresent(checkLetterIsInSent));
    }

    @Test
    public void testSendLetterAndLogoff() {
        waitForElementIsPresent(emailField);
        driver.findElement(emailField).clear();
        driver.findElement(emailField).sendKeys(user.getAddress());
        driver.findElement(buttonNext).click();
        waitForElementVisibility(passwordField);
        driver.findElement(passwordField).sendKeys(user.getPassword());
        driver.findElement(buttonNext).click();

        waitForElementClickable(createLetterButton);
        driver.findElement(createLetterButton).click();
        waitForElementIsPresent(dialogWindow);
        driver.findElement(addressLine).clear();
        driver.findElement(addressLine).sendKeys(testLetter.getAddress());
        driver.findElement(subjectLine).clear();
        driver.findElement(subjectLine).sendKeys(testLetter.getSubject());
        driver.findElement(messageBody).clear();
        driver.findElement(messageBody).sendKeys(testLetter.getMessage());
        driver.findElement(saveAndClose).click();
        waitForElementClickable(drafts);
        driver.findElement(drafts).click();

        waitForElementClickable(firstDraft);
        driver.findElement(firstDraft).click();
        waitForElementIsPresent(addressForCheck);
        waitForElementIsPresent(subjectForCheck);
        waitForElementIsPresent(messageBody);
        waitForElementClickable(sendButton);
        driver.findElement(sendButton).click();

        waitForElementIsPresent(signOutButton);
        driver.get(driver.findElement(signOutButton).getAttribute("href"));
        waitForElementIsPresent(logoffPage);

        Assert.assertTrue(elementIsPresent(logoffPage));
    }
}