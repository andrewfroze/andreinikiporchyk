package com.training.an.AThw_5.tests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class LoginTestBySelenide {

    @Test
    public void testLogin() {
        open("https://www.gmail.com/");
        $(By.name("identifier")).setValue("ForSendingLetters@gmail.com").pressEnter();
        $(By.name("password")).setValue("Sender123").pressEnter();
        $("a.gb_D.gb_Na.gb_i").shouldBe(visible).click();
        $("div.gb_1a.gb_F.gb_l.gb_2a.gb_ma>div>div>div.gb_ob").shouldHave(text("forsendingletters@gmail.com"));
    }
}
