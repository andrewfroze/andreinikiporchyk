package com.training.an.AThw_5.po;

import com.training.an.AThw_5.utils.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SentPage extends Page {
    private static final By checkLetterIsInSent = By.xpath("//tr[@role='row']");

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public By getCheckLetterIsInSent() {
        WaitUtils.waitForElementIsPresent(driver, checkLetterIsInSent);
        return checkLetterIsInSent;
    }
}
