package com.training.an.AThw_5.tests;

import com.training.an.AThw_5.po.LoginPage;
import com.training.an.AThw_5.po.LogoffPage;
import com.training.an.AThw_5.po.PostPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class WorkWithAccountTest extends ConfigurationTest {

    @Test(priority = 1)
    public void testLogin() {
        PostPage postPage = new LoginPage(driver).login(userSender).viewAccountInformation();

        Assert.assertTrue(postPage.elementIsPresent(postPage.getAccountInformation()));
    }

    @Test(priority = 2)
    public void testLoginAndLogoff() {
        PostPage postPage = new LoginPage(driver).login(userSender);
        LogoffPage logoffPage = postPage.logoff();

        Assert.assertTrue(logoffPage.elementIsPresent(logoffPage.getLogoffPageSign()));
    }
}
