package com.training.an.AThw_5.tests;

public class TestData {
    public static final String sendersEmail = "ForSendingLetters@gmail.com";
    public static final String sendersPassword = "Sender123";
    public static final String sendersFirstName = "Sender";
    public static final String sendersSecondName = "Senderov";

    public static final String addresseesEmail = "TestAccForTraining@gmail.com";
    public static final String addresseesPassword = "TestAcc123";
    public static final String addresseesFirstName = "test";
    public static final String addresseesSecondName = "test";
}
