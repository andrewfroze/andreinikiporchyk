package com.training.an.AThw_5.runner;

import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

public class TestRunner {

    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        testNG.addListener(new TestListener());
        List<String> files = Arrays.asList(
                "./src/test/resources/tests/WorkWithAccountTest.xml",
                "./src/test/resources/tests/WorkWithLettersTest.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}
