package com.training.an.AThw_5.po;

import com.training.an.AThw_5.utils.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public abstract class Page {
    protected final WebDriver driver;
    protected static final By accountInformation = By.xpath("//div[@aria-label='Account Information']");
    protected static final By createLetterButton = By.xpath("//div[contains(text(), 'Compose') and @role='button']");
    protected static final By drafts = By.xpath("//a[text()='Drafts']");
    protected static final By sentButton = By.cssSelector("a[aria-label='Sent']");
    protected static final By accountInformationButton = By.xpath("//img[@class='gb_Da gbii']/..");
    protected static final By signOutButton = By.xpath("//a[text()='Sign out']");
    protected static final By dialogWindow = By.cssSelector("div[role='dialog']");
    protected static final By draftButtonStyle = By.xpath("//a[text()='Drafts']/../../../..");
    protected static final By sentButtonButtonStyle = By.xpath("//a[text()='Sent']/../../../..");

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public By getAccountInformation() {
        return accountInformation;
    }

    public PostPage viewAccountInformation() {
        WaitUtils.waitForElementIsPresent(driver, accountInformationButton);
        WaitUtils.waitForElementClickable(driver, accountInformationButton);
        while (!driver.findElement(accountInformation).isDisplayed()) {
            driver.findElement(accountInformationButton).click();
        }
        return new PostPage(driver);
    }

    public WriteLetterPage createNewLetter() {
        WaitUtils.waitForElementClickable(driver, createLetterButton);
        driver.findElement(createLetterButton).click();
        WaitUtils.waitForElementVisibility(driver, dialogWindow);
        return new WriteLetterPage(driver);
    }

    public DraftsPage goToDrafts() {
        WaitUtils.waitForElementVisibility(driver, drafts);
        WaitUtils.waitForElementClickable(driver, drafts);
        driver.findElement(drafts).click();
        WaitUtils.waitUntilAttributeBe(driver, draftButtonStyle, "class", "TO nZ aiq");
        return new DraftsPage(driver);
    }

    public SentPage goToSent() {
        WaitUtils.waitForElementVisibility(driver, sentButton);
        WaitUtils.waitForElementClickable(driver, sentButton);
        driver.findElement(sentButton).click();
        WaitUtils.waitUntilAttributeBe(driver, sentButtonButtonStyle, "class", "TO nZ aiq");
        return new SentPage(driver);
    }

    public LogoffPage logoff() {
        viewAccountInformation();
        WaitUtils.waitForElementVisibility(driver, signOutButton);
        WaitUtils.waitForElementClickable(driver, signOutButton);
        driver.findElement(signOutButton).click();
        WaitUtils.waitForElementIsPresent(driver, By.cssSelector("html"));
        WaitUtils.waitUntilAttributeBe(driver, By.cssSelector("html"), "class", "CMgTXc");
        return new LogoffPage(driver);
    }

    public boolean elementIsPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }
}
