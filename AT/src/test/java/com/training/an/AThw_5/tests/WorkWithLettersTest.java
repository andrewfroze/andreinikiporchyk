package com.training.an.AThw_5.tests;

import com.training.an.AThw_5.businesObjects.Letter;
import com.training.an.AThw_5.po.*;
import com.training.an.AThw_5.utils.LetterGenerator;
import org.testng.Assert;
import org.testng.annotations.Test;

public class WorkWithLettersTest extends ConfigurationTest {

    @Test
    public void testCreateNewMail() {
        PostPage postPage = new LoginPage(driver).login(userSender);
        WriteLetterPage writeLetterPage = postPage.createNewLetter().fillLetterInformation(userAddressee.getEmail(), LetterGenerator.generateLetterSubject(), LetterGenerator.generateLetterMessage());
        DraftsPage draftsPage = writeLetterPage.saveAndClose().goToDrafts();

        Assert.assertTrue(draftsPage.elementIsPresent(draftsPage.getFirstDraft()));
    }

    @Test(dependsOnMethods = "testCreateNewMail")
    public void testDraftContent() {
        PostPage postPage = new LoginPage(driver).login(userSender);
        DraftsPage draftsPage = postPage.goToDrafts();
        WriteLetterPage draftForChecking = draftsPage.openFirstDraft();

        Letter actualLetter = new Letter(draftForChecking.getDraftAddressForCheck(), draftForChecking.getDraftSubjectForCheck(), draftForChecking.getMessageBody());

        Assert.assertEquals(actualLetter, testLetter);
    }

    @Test(dependsOnMethods = "testCreateNewMail")
    public void testSendMail() {
        PostPage postPage = new LoginPage(driver).login(userSender);
        DraftsPage draftsPageBefore = postPage.goToDrafts();

        String firstDraftIdBefore = draftsPageBefore.getFirstDraftId();
        DraftsPage draftsPageAfter = draftsPageBefore.openFirstDraft().sendLetterFromDrafts();

        String firstDraftIdAfter;
        if (draftsPageAfter.elementIsPresent(draftsPageAfter.getFirstDraft())) {
            firstDraftIdAfter = draftsPageAfter.getFirstDraftId();

            Assert.assertNotEquals(firstDraftIdAfter, firstDraftIdBefore);
        } else {
            Assert.assertTrue(draftsPageAfter.elementIsPresent(draftsPageAfter.getEmptyDrafts()));
        }
    }

    @Test(dependsOnMethods = "testSendMail")
    public void testMailIsInSent() {
        PostPage postPage = new LoginPage(driver).login(userSender);
        SentPage sentPage = postPage.goToSent();

        Assert.assertTrue(sentPage.elementIsPresent(sentPage.getCheckLetterIsInSent()));
    }
}
