package com.training.an.AThw_5.po;

import com.training.an.AThw_5.utils.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DraftsPage extends Page {
    private static final By firstDraft = By.xpath("//tr[@class='zA yO'][1]/td/div[@role='link']");
    private static final By firstDraftForCheck = By.xpath("//tr[@class='zA yO'][1]/td/div[@role='link']/..");
    private static final By emptyDrafts = By.cssSelector("td.TC");

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public String getFirstDraftId() {
        return driver.findElement(firstDraftForCheck).getAttribute("id");
    }

    public By getFirstDraft() {
        return firstDraft;
    }

    public By getEmptyDrafts() {
        return emptyDrafts;
    }

    public WriteLetterPage openFirstDraft() {
        WaitUtils.waitForElementVisibility(driver, firstDraft);
        WaitUtils.waitForElementClickable(driver, firstDraft);
        driver.findElement(firstDraft).click();
        WaitUtils.waitForElementVisibility(driver, dialogWindow);
        return new WriteLetterPage(driver);
    }
}
