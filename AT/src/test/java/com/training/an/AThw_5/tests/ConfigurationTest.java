package com.training.an.AThw_5.tests;

import com.training.an.AThw_5.businesObjects.Letter;
import com.training.an.AThw_5.businesObjects.User;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class ConfigurationTest {
    protected WebDriver driver;
    private static final String TEST_URL = "https://www.gmail.com/";
    protected User userAddressee;
    protected User userSender;
    protected Letter testLetter;

    @BeforeClass
    public void createUsers() {
        userAddressee = new User(TestData.addresseesEmail, TestData.addresseesPassword, TestData.addresseesFirstName, TestData.addresseesSecondName);
        userSender = new User(TestData.sendersEmail, TestData.sendersPassword, TestData.sendersFirstName, TestData.sendersSecondName);
        testLetter = new Letter(userAddressee.getEmail());
    }

    @AfterClass
    public void removeUser() {
        userAddressee = null;
        userSender = null;
    }

    @BeforeMethod
    public void setUp() {
        if (driver == null) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get(TEST_URL);
        }
    }

    @AfterMethod
    public void reset() {
        driver.quit();
        driver = null;
    }
}
