package com.training.an.AThw_5.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitUtils {

    public static void waitForElementIsPresent(WebDriver driver, By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public static void waitForElementInvisibility(WebDriver driver, By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public static void waitForElementVisibility(WebDriver driver, By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    public static void waitForElementClickable(WebDriver driver, By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(locator));
    }

    public static void waitUntilTitleContains(WebDriver driver, String string) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.titleContains(string));
    }

    public static void waitUntilAttributeBe(WebDriver driver, By locator, String attribute, String value) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBe(locator, attribute, value));
    }
}
