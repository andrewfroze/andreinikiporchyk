package com.training.an.AThw_5.po;

import com.training.an.AThw_5.utils.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WriteLetterPage extends Page {
    private static final By dialogWindow = By.cssSelector("div[role='dialog']");
    private static final By addressLine = By.xpath("//div[@aria-label='New Message']/descendant::textarea[@aria-label='To']");
    private static final By subjectLine = By.cssSelector("input[name='subjectbox']");
    private static final By messageBody = By.xpath("//div[@aria-label='Message Body']");
    private static final By saveAndClose = By.cssSelector("img[data-tooltip='Save & close']");
    private static final By draftAddressForCheck = By.xpath("//input[@name='to']/../span");
    private static final By draftSubjectForCheck = By.cssSelector("input[name='subject']");
    private static final By sendButton = By.xpath("//div[@role='button' and contains(text(),'Send')]");

    public WriteLetterPage(WebDriver driver) {
        super(driver);
    }

    public String getDraftAddressForCheck() {
        return driver.findElement(draftAddressForCheck).getAttribute("email");
    }

    public String getDraftSubjectForCheck() {
        return driver.findElement(draftSubjectForCheck).getAttribute("value");
    }

    public String getMessageBody() {
        return driver.findElement(messageBody).getText();
    }

    public WriteLetterPage fillLetterInformation(String address, String subject, String message) {
        WaitUtils.waitForElementVisibility(driver, dialogWindow);
        driver.findElement(addressLine).clear();
        driver.findElement(addressLine).sendKeys(address);
        driver.findElement(subjectLine).clear();
        driver.findElement(subjectLine).sendKeys(subject);
        driver.findElement(messageBody).clear();
        driver.findElement(messageBody).sendKeys(message);
        return new WriteLetterPage(driver);
    }

    public PostPage saveAndClose() {
        WaitUtils.waitForElementClickable(driver, saveAndClose);
        driver.findElement(saveAndClose).click();
        WaitUtils.waitForElementInvisibility(driver, dialogWindow);
        return new PostPage(driver);
    }

    public DraftsPage sendLetterFromDrafts() {
        WaitUtils.waitForElementIsPresent(driver, draftAddressForCheck);
        WaitUtils.waitForElementIsPresent(driver, draftSubjectForCheck);
        WaitUtils.waitForElementIsPresent(driver, messageBody);
        WaitUtils.waitForElementClickable(driver, sendButton);
        driver.findElement(sendButton).click();
        return new DraftsPage(driver);
    }
}
