package com.training.an.AThw_5.utils;

public class LetterGenerator {

    public static String generateLetterSubject() {
        return "test subject" + System.currentTimeMillis();
    }

    public static String generateLetterMessage() {
        return "test message" + System.currentTimeMillis();
    }
}
