package com.training.an.AThw_5.po;

import com.training.an.AThw_5.utils.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LogoffPage extends Page {
    private static final By logoffPageSign = By.xpath("//span[text()='Выберите аккаунт']");

    public LogoffPage(WebDriver driver) {
        super(driver);
    }

    public By getLogoffPageSign() {
        WaitUtils.waitForElementIsPresent(driver, logoffPageSign);
        return logoffPageSign;
    }
}
