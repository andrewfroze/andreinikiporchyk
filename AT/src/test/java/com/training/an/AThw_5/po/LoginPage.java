package com.training.an.AThw_5.po;

import com.training.an.AThw_5.businesObjects.User;
import com.training.an.AThw_5.utils.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Page {
    private static final By emailField = By.cssSelector("input[type='email']");
    private static final By buttonNext = By.xpath("//span[text()='Далее']/..");
    private static final By passwordField = By.cssSelector("input[type='password']");
    private static final By hiddenEmail = By.xpath("//input[@id='hiddenEmail']");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage enterEmail(String email) {
        driver.findElement(emailField).clear();
        driver.findElement(emailField).sendKeys(email);
        return new LoginPage(driver);
    }

    public LoginPage submitEmail() {
        driver.findElement(buttonNext).click();
        WaitUtils.waitForElementIsPresent(driver, hiddenEmail);
        WaitUtils.waitForElementVisibility(driver, passwordField);
        return new LoginPage(driver);
    }

    public LoginPage enterPassword(String password) {
        driver.findElement(passwordField).clear();
        driver.findElement(passwordField).sendKeys(password);
        return new LoginPage(driver);
    }

    public PostPage submitPassword(String email) {
        driver.findElement(buttonNext).click();
        WaitUtils.waitUntilTitleContains(driver, "- " + email.toLowerCase() + " - Gmail");
        return new PostPage(driver);
    }

    public PostPage login(User user) {
        enterEmail(user.getEmail());
        submitEmail();
        enterPassword(user.getPassword());
        return submitPassword(user.getEmail());
    }
}
