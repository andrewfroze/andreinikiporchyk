package com.training.an.AThw_9.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class EbayResultPage extends AbstractPage {
    private final By listOfProducts = By.xpath("//*[@id='mainContent']//li[contains(@class,'s-item')]//h3/..");

    public EbayProductPage clickOnFirstProduct() {
        driver.findElements(listOfProducts).get(0).click();
        logger.debug("click on first product");
        return new EbayProductPage();
    }

    public List<WebElement> getSearchResults() {
        return driver.findElements(listOfProducts);
    }

}
