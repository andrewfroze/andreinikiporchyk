package com.training.an.AThw_9.runner;

import com.training.an.AThw_9.utils.logger.LogHelper;
import com.training.an.AThw_9.utils.screenshotUtils.ScreenshotMaker;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

public class CustomRunListener extends RunListener {
    private Logger logger = LogHelper.getLogger(CustomRunListener.class);

    static {
        PropertyConfigurator.configure("./src/test/resources/log4j.properties");
    }

    @Override
    public void testFinished(Description description) {
        if (description.isTest() && description.getMethodName() != null) {
            ScreenshotMaker.saveScreenShot(description.getMethodName().replaceAll("\\W", " "));
            logger.info("Test " + description.getMethodName() + " was finished");
        }
    }

    public void testFailure(Failure failure) {
        if (failure.getDescription().isTest() && failure.getDescription().getMethodName() != null) {
            logger.error("Test " + failure.getDescription().getMethodName() + " was failed");
            ScreenshotMaker.saveScreenShot("FAILURE!!! " + failure.getDescription().getMethodName().replaceAll("\\W", " "));
        }
    }

    public void testStarted(Description description) throws Exception {
        if (description.isTest() && description.getMethodName() != null) {
            logger.debug("Test " + description.getMethodName() + " was started");
        }
    }

    public void testIgnored(Description description) throws Exception {
        if (description.isTest() && description.getMethodName() != null) {
            logger.warn("Test " + description.getMethodName() + " was ignored");
        }
    }
}
