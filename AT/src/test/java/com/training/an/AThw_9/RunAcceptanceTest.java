package com.training.an.AThw_9;

import com.training.an.AThw_9.browser.Browser;
import com.training.an.AThw_9.runner.TestRunner;
import cucumber.api.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(TestRunner.class)
@CucumberOptions(
        plugin = {
                "pretty",
                "json:target/Cucumber.json",
                "html:target/cucumber-html-report"
        }, tags = {"@smoke"}
)
public class RunAcceptanceTest {

    @AfterClass
    public static void closeDriver() {
        Browser.close();
    }
}
