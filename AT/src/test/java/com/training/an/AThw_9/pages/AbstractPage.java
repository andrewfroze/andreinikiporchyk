package com.training.an.AThw_9.pages;

import com.training.an.AThw_9.browser.Browser;
import com.training.an.AThw_9.utils.logger.LogHelper;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;

public abstract class AbstractPage {
    protected WebDriver driver = Browser.getWebDriverInstance();
    Logger logger = LogHelper.getLogger(this.getClass());

    static {
        PropertyConfigurator.configure("./src/test/resources/log4j.properties");
    }
}
