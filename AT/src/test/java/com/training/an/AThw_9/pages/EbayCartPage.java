package com.training.an.AThw_9.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class EbayCartPage extends AbstractPage {
    private static final By itemInCart = By.xpath("//*[@class='cart-bucket']//h3//span[@class='BOLD']");
    private static final By numberOfItems = By.xpath("//header//li[@id='gh-minicart-hover']//i");

    public WebElement getLastAddedItemNameElement() {
        return driver.findElement(itemInCart);
    }

    public WebElement getNumberOfAddedItemsElement() {
        return driver.findElement(numberOfItems);
    }
}
