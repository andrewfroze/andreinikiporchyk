package com.training.an.AThw_9.utils.logger;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LogHelper {
    private static boolean root = false;

    public static Logger getLogger(Class cls) {
        if (root) {
            return LogManager.getLogger(cls);
        }
        PropertyConfigurator.configure("./src/test/resources/log4j.properties");
        root = true;
        return LogManager.getLogger(cls);
    }
}
