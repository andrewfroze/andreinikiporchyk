package com.training.an.AThw_9.browser;

import com.training.an.AThw_9.utils.logger.LogHelper;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;

import java.util.concurrent.TimeUnit;

public class Browser {
    private static WebDriver driver;
    static Logger logger = LogHelper.getLogger(Browser.class);

    static {
        PropertyConfigurator.configure("./src/test/resources/log4j.properties");
    }

    public static WebDriver getWebDriverInstance() {
        if (driver != null) {
            logger.debug("returned existing driver");
            return driver;
        }
        return driver = init();
    }

    public static WebDriver init() {
        String browserType = (System.getProperty("browserType") == null ? "chrome" : System.getProperty("browserType"));
        if (driver == null) {
            switch (browserType) {
                case "edge": {
                    WebDriverManager.edgedriver().setup();
                    driver = new EdgeDriver();
                    break;
                }
                case "opera": {
                    WebDriverManager.operadriver().setup();
                    driver = new OperaDriver();
                }
                case "firefox": {
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver();
                    break;
                }
                case "chrome":
                default: {
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                    break;
                }
            }
            logger.debug("Starting new" + browserType + "browser");
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }
        return driver;
    }

    public static void close() {
        try {
            driver.quit();
            logger.debug("Browser closed");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            ex.printStackTrace();
        } finally {
            driver = null;
            logger.debug("driver nulled");
        }
    }
}
