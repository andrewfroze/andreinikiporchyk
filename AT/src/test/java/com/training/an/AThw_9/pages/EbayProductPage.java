package com.training.an.AThw_9.pages;

import com.training.an.AThw_9.utils.screenshotUtils.ScreenshotMaker;
import com.training.an.AThw_9.utils.waitUtils.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.List;

public class EbayProductPage extends AbstractPage {
    private final By addToCart = By.xpath("//a[@id='isCartBtn_btn']");
    private final By itemTitle = By.xpath("//h1[@id='itemTitle']");
    private final By selectFieldsLocator = By.xpath("//select");
    private int numberOfOptionValue = 0;
    private final By selectFieldsOption = By.xpath("//select/option[2]");


    public WebElement getSelectedProductFullNameElement() {
        return driver.findElement(itemTitle);
    }

    public EbayProductPage selectFirstOptionInAllSelectFields() {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(itemTitle));
        logger.debug("page scrolled to product name");
        List<WebElement> selectFields = driver.findElements(selectFieldsLocator);
        String itemIsDisabled;
        WebElement optionToClick;
        if (selectFields.size() > 2) {
            for (int i = 1, l = selectFields.size(); i < l - 1; i++) {
                selectFields.get(i).click();
                List<WebElement> firstOption = driver.findElements(selectFieldsOption);
                itemIsDisabled = firstOption.get(i).getAttribute("disabled");
                optionToClick = firstOption.get(i);
                while (itemIsDisabled != null && itemIsDisabled.equals("true")) {
                    numberOfOptionValue = Integer.parseInt(firstOption.get(i).getAttribute("value"));
                    numberOfOptionValue += 1;
                    optionToClick = driver.findElement(By.xpath("//select/option[@value=" + numberOfOptionValue + "]"));
                    itemIsDisabled = optionToClick.getAttribute("disabled");
                }
                optionToClick.click();
                firstOption = driver.findElements(selectFieldsOption);
                logger.debug("selected" + i + "/" + l + "option");
            }
        }
        ScreenshotMaker.colorElement(driver.findElement(itemTitle), "red");
        logger.debug("colored product name");
        return new EbayProductPage();
    }

    public EbayCartPage addProductToCart() {
        WaitUtils.waitForElementVisible(driver, addToCart);
        driver.findElement(addToCart).click();
        logger.debug("add to cart button click");
        return new EbayCartPage();
    }
}
