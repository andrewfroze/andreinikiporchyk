package com.training.an.AThw_9.steps;

import com.training.an.AThw_9.browser.Browser;
import com.training.an.AThw_9.pages.EbayCartPage;
import com.training.an.AThw_9.pages.EbayHomePage;
import com.training.an.AThw_9.pages.EbayProductPage;
import com.training.an.AThw_9.pages.EbayResultPage;
import com.training.an.AThw_9.utils.screenshotUtils.ScreenshotMaker;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

import static org.hamcrest.CoreMatchers.containsString;

public class EbayStepDefs {
    private static String productNameForAssertion;
    private static final String cartURL = "https://cart.payments.ebay.com/";
    private static EbayProductPage ebayProductPage;

    @Given("^I opened Ebay Page$")
    public void iOpenedEbayPage() {
        new EbayHomePage().open();
    }

    @When("^I search the product \"([^\"]*)\"$")
    public void iSearchTheProduct(String query) {
        new EbayHomePage().enterQuery(query).clickSubmit();
    }

    @Then("^the term query \"([^\"]*)\" should be the first in the Search Result grid$")
    public void theTermQueryShouldBeTheFirstInTheSearchResultGrid(String expectPhrase) {
        EbayResultPage actualPage = new EbayResultPage();
        String actualName = actualPage.getSearchResults().get(0).getText().toLowerCase();

        Assert.assertThat(actualName, containsString(expectPhrase.toLowerCase()));

        ScreenshotMaker.colorElement(actualPage.getSearchResults().get(0), "red");
    }

    @When("^I open first product's page$")
    public void iOpenFirstProductSPage() {
        ebayProductPage = new EbayResultPage().clickOnFirstProduct().selectFirstOptionInAllSelectFields();
        productNameForAssertion = ebayProductPage.getSelectedProductFullNameElement().getText();

        ScreenshotMaker.colorElement(ebayProductPage.getSelectedProductFullNameElement(), "red");
    }

    @When("^I add opened product to the cart$")
    public void iAddOpenedProductToTheCart() {
        ebayProductPage.addProductToCart();
    }

    @Then("^the Cart Page should be opened$")
    public void theCartPageShouldBeOpened() {
        String actualURL = Browser.getWebDriverInstance().getCurrentUrl();

        Assert.assertEquals(cartURL, actualURL);
    }

    @Then("^the name of added product should be the same, like the name on previous product page$")
    public void theNameOfAddedProductShouldBeTheSameLikeTheNameOnPreviousProductPage() {
        WebElement lastAddedProductNameElement = new EbayCartPage().getLastAddedItemNameElement();
        String lastAddedProductName = lastAddedProductNameElement.getText();

        Assert.assertThat(productNameForAssertion, containsString(lastAddedProductName));

        ScreenshotMaker.colorElement(lastAddedProductNameElement, "red");
    }

    @Then("^the number \"([^\"]*)\" is situated above the Cart icon$")
    public void theNumberIsSituatedAboveTheCartIcon(String itemsInCart) {
        WebElement actualNumberInCart = new EbayCartPage().getNumberOfAddedItemsElement();
        int actualItemsInCart = Integer.parseInt(actualNumberInCart.getText());

        Assert.assertEquals(actualItemsInCart, Integer.parseInt(itemsInCart));

        ScreenshotMaker.colorElement(actualNumberInCart, "green");
    }
}
