package com.training.an.AThw_9.utils.screenshotUtils;

import com.training.an.AThw_9.browser.Browser;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenshotMaker {

    public static void colorElement(WebElement elem, String color) {
        WebDriver driver = Browser.getWebDriverInstance();
        if (driver instanceof JavascriptExecutor) {
            String command = "arguments[0].style.border='3px solid " + color + "'";
            ((JavascriptExecutor) driver).executeScript(command, elem);
        }
    }

    public static void saveScreenShot(String fileN) {
        WebDriver driver = Browser.getWebDriverInstance();
        if (driver != null) {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String destinationDir = "./test-output/screenshots/";
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy-h-mm-ss-SS--a");
            String formattedDate = sdf.format(date);
            String fileName = destinationDir + (fileN.isEmpty() ? "screenshot-" : fileN + "-") + formattedDate;
            try {
                FileUtils.copyFile(scrFile, new File(String.format("./%s.png", fileName)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
