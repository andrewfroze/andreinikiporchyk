package com.training.an.AThw_9.pages;

import com.training.an.AThw_9.utils.waitUtils.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.testng.Reporter;

public class EbayHomePage extends AbstractPage {
    private static final String BASE_URL = "https://www.ebay.com/";
    private final By searchInput = By.xpath("//input[@type='text']");
    private final By submit = By.xpath("//input[@type='submit']");

    public EbayHomePage open() {
        driver.get(BASE_URL);
        logger.info("Go to the base page " + BASE_URL);
        return this;
    }

    public EbayHomePage enterQuery(String query) {
        try {
            WaitUtils.waitForElementVisible(driver, searchInput);
            driver.findElement(searchInput).sendKeys(query);
            logger.debug("Enter " + query + " in the Search field");
        } catch (WebDriverException ex) {
            logger.error(ex.getMessage());
            Reporter.log("Error in Web Driver");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }

        return new EbayHomePage();
    }

    public EbayResultPage clickSubmit() {
        try {
            WaitUtils.waitForElementVisible(driver, submit);
            driver.findElement(submit).click();
            logger.debug("Clicking the Submit button");
        } catch (WebDriverException ex) {
            logger.error(ex.getMessage());
            Reporter.log("Error in Web Driver");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return new EbayResultPage();
    }
}
