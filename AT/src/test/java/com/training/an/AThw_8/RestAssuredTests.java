package com.training.an.AThw_8;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.BeforeClass;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;

public class RestAssuredTests {
    private static final String baseUri = "http://jsonplaceholder.typicode.com/albums";
    private static int pageNumber;
    private static RequestSpecification allAlbums;
    private static RequestSpecification oneAlbum;
    private static Album albumForPost;
    private static Album albumForPut;

    @BeforeClass
    public static void setParams() {
        pageNumber = 20;
        allAlbums = given().baseUri(baseUri);
        oneAlbum = given().pathParam("pageId", pageNumber).baseUri(baseUri);
        albumForPost = new Album(1, "test album");
        albumForPut = new Album(1, "test album update");
    }

    @Test
    public void testGetAlbumsCheckStatusCode() {
        allAlbums.when()
                .get()
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    public void testGetAlbumsContentTypeIsExist() {
        allAlbums.when()
                .get()
                .then()
                .assertThat()
                .contentType(notNullValue());
    }

    @Test
    public void testGetAlbumsContentTypeRight() {
        allAlbums.when()
                .get()
                .then()
                .assertThat()
                .contentType("application/json; charset=utf-8");
    }

    @Test
    public void testGetAlbumsCheckingBodyArraySize() {
        allAlbums.when()
                .get()
                .then()
                .assertThat()
                .body("size()", equalTo(100));
    }

    @Test
    public void testGetAlbumCheckBodyContainsFieldUserId() {
        oneAlbum.when()
                .get("/{pageId}")
                .then()
                .assertThat()
                .body(containsString("userId"));
    }

    @Test
    public void testGetAlbumCheckBodyContainsFieldId() {
        oneAlbum.when()
                .get("/{pageId}")
                .then()
                .assertThat()
                .body(containsString("id"));
    }

    @Test
    public void testGetAlbumCheckBodyContainsFieldTitle() {
        oneAlbum.when()
                .get("/{pageId}")
                .then()
                .assertThat()
                .body(containsString("title"));
    }

    @Test
    public void testPostNewAlbum() {
        Response response = allAlbums.body(albumForPost)
                .when()
                .post();
        response.then()
                .assertThat()
                .statusCode(201)
                .assertThat()
                .body(notNullValue());

        System.out.println("New album's id is " + response.jsonPath().get("id"));
    }

    @Test
    public void testUpdateThePost() {
        Response response = oneAlbum.body(albumForPut)
                .when()
                .put("/{pageId}");
        response.then()
                .assertThat()
                .statusCode(200)
                .assertThat()
                .body("id", equalTo(pageNumber));
    }

    @Test
    public void testDelete() {
        oneAlbum.when()
                .delete("/{pageId}")
                .then()
                .assertThat()
                .statusCode(200);
    }
}
