package com.training.an.AThw_8;

import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

public class RestTemplateTests {
    private static final String baseUri = "http://jsonplaceholder.typicode.com/albums";
    private static int pageNumber;
    private static String allAlbumsUri;
    private static String oneAlbumUri;
    private static Album albumForPost;
    private static Album albumForPut;
    private static RestTemplate restTemplate;

    @BeforeClass
    public static void preparations() {
        pageNumber = 20;
        allAlbumsUri = baseUri;
        oneAlbumUri = baseUri +"/" + pageNumber;
        System.out.println(oneAlbumUri);
        albumForPost = new Album(1, "test album");
        albumForPut = new Album(1, "test album update");
        restTemplate = new RestTemplate();
    }

    @Test
    public void testGetAlbumsCheckStatusCode() {
        ResponseEntity<String> response = restTemplate.getForEntity(allAlbumsUri, String.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
    }

    @Test
    public void testGetAlbumsContentTypeIsExist() {
        HttpHeaders httpHeaders = restTemplate.headForHeaders(allAlbumsUri);

        Assert.assertNotNull(httpHeaders.getContentType());
    }

    @Test
    public void testGetAlbumsContentTypeRight() {
        HttpHeaders httpHeaders = restTemplate.headForHeaders(allAlbumsUri);

        Assert.assertEquals("application/json;charset=utf-8", httpHeaders.getContentType().toString());
    }

    @Test
    public void testGetAlbumsCheckingBodyArraySize() {
        ResponseEntity<Album[]> response = restTemplate.getForEntity(allAlbumsUri, Album[].class);
        Album[] albums = response.getBody();

        Assert.assertEquals(100, albums.length);
    }

    @Test
    public void testGetAlbum20CheckBodyContainsFieldUserId() {
        ResponseEntity<String> response = restTemplate.getForEntity(oneAlbumUri, String.class);

        Assert.assertThat(response.getBody(), containsString("userId"));
    }

    @Test
    public void testGetAlbum20CheckBodyContainsFieldId() {
        ResponseEntity<String> response = restTemplate.getForEntity(oneAlbumUri, String.class);

        Assert.assertThat(response.getBody(), containsString("id"));
    }

    @Test
    public void testGetAlbum20CheckBodyContainsFieldTitle() {
        ResponseEntity<String> response = restTemplate.getForEntity(oneAlbumUri, String.class);

        Assert.assertThat(response.getBody(), containsString("title"));
    }

    @Test
    public void testPostNewAlbum() {
        ResponseEntity<Album> response = restTemplate.postForEntity(allAlbumsUri, albumForPost, Album.class);

        Assert.assertEquals(201, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        System.out.println("new id is " + response.getBody().getId());
    }

    @Test
    public void testUpdateThePost() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<Album> entity = new HttpEntity<Album>(albumForPut, headers);
        ResponseEntity<Album> response = restTemplate.exchange(oneAlbumUri, HttpMethod.PUT, entity, Album.class);

        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertEquals(pageNumber, response.getBody().getId());
    }

    @Test
    public void testDeletePost() {
        Response response = given().when().delete(oneAlbumUri);
        Assert.assertEquals(200, response.getStatusCode());
    }
}
