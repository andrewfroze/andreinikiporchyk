package com.training.an.AThw_3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {
    protected final WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    protected void waitForElementIsPresent(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    protected void waitForElementInvisibility(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    protected void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    protected void waitForElementClickable(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(locator));
    }

    protected void waitForTitleContains (String title) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.titleContains(title));
    }

    protected void waitForAttributeToBe (By locator,String attribute, String value) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBe(locator, attribute, value));
    }

    protected boolean elementIsPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }
}
