package com.training.an.AThw_3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends Page {
    private static final By emailField = By.cssSelector("input[type='email']");
    private static final By buttonNext = By.xpath("//span[text()='Далее']/..");
    private static final By passwordField = By.cssSelector("input[type='password']");
    private static final By hiddenEmail = By.xpath("//input[@id='hiddenEmail']");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage enterEmail(String email) {
        driver.findElement(emailField).clear();
        driver.findElement(emailField).sendKeys(email);
        return new LoginPage(driver);
    }

    public LoginPage submitEmail() {
        driver.findElement(buttonNext).click();
        waitForElementIsPresent(hiddenEmail);
        waitForElementVisibility(passwordField);
        return new LoginPage(driver);
    }

    public LoginPage enterPassword(String email) {
        driver.findElement(passwordField).clear();
        driver.findElement(passwordField).sendKeys(email);
        return new LoginPage(driver);
    }

    public PostPage submitPassword(String email) {
        driver.findElement(buttonNext).click();
        waitForTitleContains("- " + email + " - Gmail");
        return new PostPage(driver);
    }

    public PostPage login(String email, String password) {
        enterEmail(email);
        submitEmail();
        enterPassword(password);
        return submitPassword(email);
    }
}
