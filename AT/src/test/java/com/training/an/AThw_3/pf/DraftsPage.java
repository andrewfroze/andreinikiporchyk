package com.training.an.AThw_3.pf;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DraftsPage extends Menu {

    @FindBy(xpath = "//tr[@class='zA yO'][1]/td/div[@role='link']")
    private WebElement firstDraft;

    @FindBy(xpath = "//tr[@class='zA yO'][1]/td/div[@role='link']/..")
    private WebElement firstDraftForCheck;

    @FindBy(css = "td.TC")
    private WebElement emptyDrafts;

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getFirstDraft() {
        return firstDraft;
    }

    public WebElement getEmptyDrafts() {
        return emptyDrafts;
    }

    public String getFirstDraftId (){
        return firstDraftForCheck.getAttribute("id");
    }

    public WriteLetterPage openFirstDraft() {
        waitForElementVisibility(firstDraft);
        waitForElementClickable(firstDraft);
        firstDraft.click();
        waitForElementVisibility(dialogWindow);
        return new WriteLetterPage(driver);
    }
}
