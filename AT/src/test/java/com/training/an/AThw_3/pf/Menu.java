package com.training.an.AThw_3.pf;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class Menu extends Page {

    @FindBy(xpath = "//div[@aria-label='Account Information']")
    private WebElement accountInformation;

    @FindBy(xpath = "//div[contains(text(), 'Compose') and @role='button']")
    private WebElement createLetterButton;

    @FindBy(xpath = "//a[@aria-label='Drafts']")
    private WebElement drafts;

    @FindBy(xpath = "//div[@aria-label='Account Information']//a[@target='_top']")
    private WebElement signOutButton;

    @FindBy(css = "a[aria-label='Sent']")
    private WebElement sentButton;

    @FindBy(xpath = "//img[@class='gb_Da gbii']/..")
    private WebElement accountInformationButton;

    @FindBy(css = "div[role='dialog']")
    protected WebElement dialogWindow;

    @FindBy(xpath = "//a[text()='Drafts']/../../../..")
    private WebElement draftButtonStyle;

    @FindBy(xpath = "//a[text()='Sent']/../../../..")
    private WebElement sentButtonButtonStyle;

    public Menu(WebDriver driver) {
        super(driver);
    }

    public WebElement getAccountInformation() {
        return accountInformation;
    }

    public PostPage viewAccountInformation() {
        waitForElementVisibility(accountInformationButton);
        waitForElementClickable(accountInformationButton);
        while (!accountInformation.isDisplayed()) {
            accountInformationButton.click();
        }
        return new PostPage(driver);
    }

    public WriteLetterPage createNewLetter() {
        waitForElementClickable(createLetterButton);
        createLetterButton.click();
        waitForElementVisibility(dialogWindow);
        return new WriteLetterPage(driver);
    }

    public DraftsPage goToDrafts() {
        waitForElementVisibility(drafts);
        waitForElementClickable(drafts);
        drafts.click();
        waitForAttributeToBe(draftButtonStyle, "class", "TO nZ aiq");
        return new DraftsPage(driver);
    }

    public SentPage goToSent() {
        waitForElementVisibility(sentButton);
        waitForElementClickable(sentButton);
        sentButton.click();
        waitForAttributeToBe(sentButtonButtonStyle, "class", "TO nZ aiq");
        return new SentPage(driver);
    }

    public LogoffPage logoff() {
        viewAccountInformation();
        waitForElementVisibility(signOutButton);
        waitForElementClickable(signOutButton);
        signOutButton.click();
        waitForElementIsPresent(By.cssSelector("html"));
        waitForAttributeToBe(driver.findElement(By.cssSelector("html")), "class", "CMgTXc");
        return new LogoffPage(driver);
    }
}
