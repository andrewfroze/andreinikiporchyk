package com.training.an.AThw_3.pf;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {
    protected final WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    protected void waitForElementInvisibility(WebElement element) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOf(element));
    }

    protected void waitForElementVisibility(WebElement element) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(element));
    }

    protected void waitForElementClickable(WebElement element) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(element));
    }

    protected boolean elementIsPresent(WebElement element) {
        return element.getSize().height > 0 && element.getSize().width > 0;
    }

    protected void waitForTextToBe(By locator, String text) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.textToBe(locator, text));
    }

    protected void waitForTitleContains (String title) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.titleContains(title));
    }

    protected void waitForAttributeToBe (WebElement element,String attribute, String value) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.attributeToBe(element, attribute, value));
    }

    protected void waitForElementIsPresent(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(locator));
    }
}
