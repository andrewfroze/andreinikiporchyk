package com.training.an.AThw_3.pf;

import java.util.Objects;

    public class Letter {
        private String subject;
        private String message;
        private String address;

        public Letter(String address) {
            this.subject = "subject" + System.currentTimeMillis();
            this.message = "message" + System.currentTimeMillis();
            this.address = address;
        }

        public Letter(String address, String subject, String message) {
            this.subject = subject;
            this.message = message;
            this.address = address;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Letter letter = (Letter) o;
            return subject.equals(letter.subject) &&
                    message.equals(letter.message) &&
                    address.equals(letter.address);
        }

        @Override
        public int hashCode() {
            return Objects.hash(subject, message, address);
        }
    }