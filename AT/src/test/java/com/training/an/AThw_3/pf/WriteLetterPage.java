package com.training.an.AThw_3.pf;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WriteLetterPage extends Page {

    @FindBy(xpath = "//div[@aria-label='New Message']/descendant::textarea[@aria-label='To']")
    private WebElement addressLine;

    @FindBy(xpath = "//div[@role='button' and contains(text(),'Send')]")
    private WebElement sendButton;

    @FindBy(xpath = "//div[@aria-label='Message Body']")
    private WebElement messageBody;

    @FindBy(xpath = "//input[@name='to']/../span")
    private WebElement draftAddressForCheck;

    @FindBy(css = "div[role='dialog']")
    private WebElement dialogWindow;

    @FindBy(css = "input[name='subjectbox']")
    private WebElement subjectLine;

    @FindBy(css = "img[data-tooltip='Save & close']")
    private WebElement saveAndClose;

    @FindBy(css = "input[name='subject']")
    private WebElement draftSubjectForCheck;

    public WriteLetterPage(WebDriver driver) {
        super(driver);
    }

    public String getDraftAddressForCheck() {
        return draftAddressForCheck.getAttribute("email");
    }

    public String getDraftSubjectForCheck() {
        return draftSubjectForCheck.getAttribute("value");
    }

    public String getMessageBody() {
        return messageBody.getText();
    }

    public WriteLetterPage fillLetterInformation(Letter letter) {
        waitForElementVisibility(dialogWindow);
        addressLine.clear();
        addressLine.sendKeys(letter.getAddress());
        subjectLine.clear();
        subjectLine.sendKeys(letter.getSubject());
        messageBody.clear();
        messageBody.sendKeys(letter.getMessage());
        return new WriteLetterPage(driver);
    }

    public PostPage saveAndClose() {
        waitForElementClickable(saveAndClose);
        saveAndClose.click();
        waitForElementInvisibility(dialogWindow);
        return new PostPage(driver);
    }

    public DraftsPage sendLetterFromDrafts() {
        waitForElementVisibility(dialogWindow);
        waitForElementVisibility(sendButton);
        waitForElementClickable(sendButton);
        sendButton.click();
        waitForElementInvisibility(dialogWindow);
        return new DraftsPage(driver);
    }
}
