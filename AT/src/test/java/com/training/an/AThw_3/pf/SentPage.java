package com.training.an.AThw_3.pf;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SentPage extends Menu {

    @FindBy(xpath = "//table[@class='F cf zt']/tbody/tr[1]")
    private WebElement checkLetterIsInSent;

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getCheckLetterIsInSent() {
        waitForElementVisibility(checkLetterIsInSent);
        return checkLetterIsInSent;
    }
}
