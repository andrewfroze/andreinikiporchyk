package com.training.an.AThw_3.pf;

import com.training.an.AThw_3.po.Letter;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GmailTest extends ConfigurationTest {

    @Test(priority = 1)
    public void testLogin() {
        PostPage postPage = new LoginPage(driver).login(user.getAddress(), user.getPassword()).viewAccountInformation();

        Assert.assertTrue(postPage.elementIsPresent(postPage.getAccountInformation()));
    }

    @Test(priority = 2)
    public void testCreateNewMail() {
        PostPage postPage = new LoginPage(driver).login(user.getAddress(), user.getPassword());
        WriteLetterPage writeLetterPage = postPage.createNewLetter().fillLetterInformation(testLetter);
        DraftsPage draftsPage = writeLetterPage.saveAndClose().goToDrafts();

        Assert.assertTrue(draftsPage.elementIsPresent(draftsPage.getFirstDraft()));
    }

    @Test(dependsOnMethods = "testCreateNewMail")
    public void testDraftContent() {
        PostPage postPage = new LoginPage(driver).login(user.getAddress(), user.getPassword());
        DraftsPage draftsPage = postPage.goToDrafts();
        WriteLetterPage draftForChecking = draftsPage.openFirstDraft();

        Letter actualLetter = new Letter(draftForChecking.getDraftAddressForCheck(), draftForChecking.getDraftSubjectForCheck(), draftForChecking.getMessageBody());

        Assert.assertEquals(actualLetter, testLetter);
    }

    @Test(dependsOnMethods = "testCreateNewMail")
    public void testSendMail() {
        PostPage postPage = new LoginPage(driver).login(user.getAddress(), user.getPassword());
        DraftsPage draftsPageBefore = postPage.goToDrafts();

        String firstDraftIdBefore = draftsPageBefore.getFirstDraftId();

        DraftsPage draftsPageAfter = draftsPageBefore.openFirstDraft().sendLetterFromDrafts();

        String firstDraftIdAfter;
        if (draftsPageAfter.elementIsPresent(draftsPageAfter.getFirstDraft())) {
            firstDraftIdAfter = draftsPageAfter.getFirstDraftId();
            Assert.assertNotEquals(firstDraftIdAfter, firstDraftIdBefore);
        } else {
            Assert.assertTrue(draftsPageAfter.elementIsPresent(draftsPageAfter.getEmptyDrafts()));
        }
    }


    @Test(dependsOnMethods = "testSendMail")
    public void testMailIsInSent() {
        PostPage postPage = new LoginPage(driver).login(user.getAddress(), user.getPassword());
        SentPage sentPage = postPage.goToSent();

        Assert.assertTrue(sentPage.elementIsPresent(sentPage.getCheckLetterIsInSent()));
    }

    @Test(priority = 3)
    public void testLoginAndLogoff() {
        PostPage postPage = new LoginPage(driver).login(user.getAddress(), user.getPassword());
        LogoffPage logoffPage = postPage.logoff();

        Assert.assertTrue(logoffPage.elementIsPresent(logoffPage.getLogoffPageSign()));
    }
}
