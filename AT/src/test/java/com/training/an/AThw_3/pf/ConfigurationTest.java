package com.training.an.AThw_3.pf;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class ConfigurationTest {
    protected WebDriver driver;
    private static final String TEST_URL = "https://www.gmail.com/";
    protected static Letter testLetter;
    protected static User user;

    @BeforeClass
    public void generateNewUniqueLetter() {
        user = new User("TestAccForTraining@gmail.com", "TestAcc123", "test", "test");
        testLetter = new Letter(user.getAddress());
    }

    @BeforeMethod
    public void setUp() {
        if (driver == null) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.manage().window().maximize();
            driver.get(TEST_URL);
        }
    }

    @AfterMethod
    public void reset() {
        driver.quit();
        driver = null;
    }
}
