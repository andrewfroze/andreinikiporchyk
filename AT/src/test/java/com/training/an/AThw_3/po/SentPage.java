package com.training.an.AThw_3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SentPage extends Menu {
    private static final By checkLetterIsInSent = By.xpath("//table[@class='F cf zt']/tbody/tr[1]");

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public By getCheckLetterIsInSent() {
        waitForElementIsPresent(checkLetterIsInSent);
        return checkLetterIsInSent;
    }
}
