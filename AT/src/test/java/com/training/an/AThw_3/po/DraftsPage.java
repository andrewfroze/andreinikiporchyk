package com.training.an.AThw_3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DraftsPage extends Menu {
    private static final By firstDraft = By.xpath("//tr[@class='zA yO'][1]/td/div[@role='link']");
    private static final By firstDraftForCheck = By.xpath("//tr[@class='zA yO'][1]/td/div[@role='link']/..");
    private static final By emptyDrafts = By.cssSelector("td.TC");


    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public By getFirstDraft() {
        return firstDraft;
    }

    public By getEmptyDrafts() {
        return emptyDrafts;
    }

    public String getFirstDraftId (){
        return driver.findElement(firstDraftForCheck).getAttribute("id");
    }

    public WriteLetterPage openFirstDraft() {
        waitForElementVisibility(firstDraft);
        waitForElementClickable(firstDraft);
        driver.findElement(firstDraft).click();
        waitForElementVisibility(dialogWindow);
        return new WriteLetterPage(driver);
    }
}
