package com.training.an.AThw_3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WriteLetterPage extends Page {
    private static final By dialogWindow = By.cssSelector("div[role='dialog']");
    private static final By addressLine = By.xpath("//div[@aria-label='New Message']/descendant::textarea[@aria-label='To']");
    private static final By subjectLine = By.cssSelector("input[name='subjectbox']");
    private static final By messageBody = By.xpath("//div[@aria-label='Message Body']");
    private static final By saveAndClose = By.cssSelector("img[data-tooltip='Save & close']");
    private static final By draftAddressForCheck = By.xpath("//input[@name='to']/../span");
    private static final By draftSubjectForCheck = By.cssSelector("input[name='subject']");
    private static final By sendButton = By.xpath("//div[@role='button' and contains(text(),'Send')]");


    public WriteLetterPage(WebDriver driver) {
        super(driver);
    }

    public String getDraftAddressForCheck() {
        return driver.findElement(draftAddressForCheck).getAttribute("email");
    }

    public String getDraftSubjectForCheck() {
        return driver.findElement(draftSubjectForCheck).getAttribute("value");
    }

    public String getMessageBody() {
        return driver.findElement(messageBody).getText();
    }

    public WriteLetterPage fillLetterInformation(Letter letter) {
        waitForElementVisibility(dialogWindow);
        driver.findElement(addressLine).clear();
        driver.findElement(addressLine).sendKeys(letter.getAddress());
        driver.findElement(subjectLine).clear();
        driver.findElement(subjectLine).sendKeys(letter.getSubject());
        driver.findElement(messageBody).clear();
        driver.findElement(messageBody).sendKeys(letter.getMessage());
        return new WriteLetterPage(driver);
    }

    public PostPage saveAndClose() {
        waitForElementClickable(saveAndClose);
        driver.findElement(saveAndClose).click();
        waitForElementInvisibility(dialogWindow);
        return new PostPage(driver);
    }

    public DraftsPage sendLetterFromDrafts() {
        waitForElementVisibility(dialogWindow);
        waitForElementIsPresent(draftAddressForCheck);
        waitForElementIsPresent(draftSubjectForCheck);
        waitForElementIsPresent(messageBody);
        waitForElementIsPresent(sendButton);
        waitForElementClickable(sendButton);
        driver.findElement(sendButton).click();
        return new DraftsPage(driver);
    }
}
