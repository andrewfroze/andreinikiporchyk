package com.training.an.AThw_3.pf;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends Page {

    @FindBy(xpath = "//span[text()='Далее']/..")
    private WebElement buttonNext;

    @FindBy(css = "input[type='email']")
    private WebElement emailField;

    @FindBy(css = "input[type='password']")
    private WebElement passwordField;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage enterEmail(String email) {
        emailField.clear();
        emailField.sendKeys(email);
        return new LoginPage(driver);
    }

    public LoginPage submitEmail() {
        buttonNext.click();
        waitForTextToBe(By.xpath("//h1/span"), "Добро пожаловать!");
        waitForElementVisibility(passwordField);
        return new LoginPage(driver);
    }

    public LoginPage enterPassword(String email) {
        passwordField.clear();
        passwordField.sendKeys(email);
        return new LoginPage(driver);
    }

    public PostPage submitPassword(String email) {
        buttonNext.click();
        waitForTitleContains("- " + email + "- Gmail");
        return new PostPage(driver);
    }

    public PostPage login(String email, String password) {
        enterEmail(email);
        submitEmail();
        enterPassword(password);
        return submitPassword(email);
    }
}
