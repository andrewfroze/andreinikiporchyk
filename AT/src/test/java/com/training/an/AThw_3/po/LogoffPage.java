package com.training.an.AThw_3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogoffPage extends Page {
    private static final By logoffPageSign = By.xpath("//h1[@class='ahT6S ']");

    public LogoffPage(WebDriver driver) {
        super(driver);
    }

    public By getLogoffPageSign() {
        waitForElementIsPresent(logoffPageSign);
        return logoffPageSign;
    }
}
