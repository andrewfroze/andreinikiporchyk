package com.training.an.AThw_3.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Menu extends Page {
    protected static final By accountInformation = By.xpath("//div[@aria-label='Account Information']");
    protected static final By createLetterButton = By.xpath("//div[contains(text(), 'Compose') and @role='button']");
    protected static final By drafts = By.xpath("//a[@aria-label='Drafts']");
    protected static final By sentButton = By.cssSelector("a[aria-label='Sent']");
    protected static final By accountInformationButton = By.xpath("//img[@class='gb_Da gbii']/..");
    protected static final By signOutButton = By.xpath("//div[@aria-label='Account Information']//a[@target='_top']");
    protected static final By dialogWindow = By.cssSelector("div[role='dialog']");
    protected static final By draftButtonStyle = By.xpath("//a[@aria-label='Drafts']/../../../..");
    protected static final By sentButtonButtonStyle = By.xpath("//a[text()='Sent']/../../../..");

    public Menu(WebDriver driver) {
        super(driver);
    }

    public By getAccountInformation() {
        return accountInformation;
    }

    public PostPage viewAccountInformation() {
        waitForElementIsPresent(accountInformationButton);
        waitForElementClickable(accountInformationButton);
        while (!driver.findElement(accountInformation).isDisplayed()) {
            driver.findElement(accountInformationButton).click();
        }
        return new PostPage(driver);
    }

    public WriteLetterPage createNewLetter() {
        waitForElementClickable(createLetterButton);
        driver.findElement(createLetterButton).click();
        waitForElementVisibility(dialogWindow);
        return new WriteLetterPage(driver);
    }

    public DraftsPage goToDrafts() {
        waitForElementVisibility(drafts);
        waitForElementClickable(drafts);
        driver.findElement(drafts).click();
        waitForAttributeToBe(draftButtonStyle, "class", "TO nZ aiq");
        return new DraftsPage(driver);
    }

    public SentPage goToSent() {
        waitForElementVisibility(sentButton);
        waitForElementClickable(sentButton);
        driver.findElement(sentButton).click();
        waitForAttributeToBe(sentButtonButtonStyle, "class", "TO nZ aiq");
        return new SentPage(driver);
    }

    public LogoffPage logoff() {
        viewAccountInformation();
        waitForElementVisibility(signOutButton);
        waitForElementClickable(signOutButton);
        driver.findElement(signOutButton).click();
        waitForElementIsPresent(By.cssSelector("html"));
        waitForAttributeToBe(By.cssSelector("html"), "class", "CMgTXc");
        return new LogoffPage(driver);
    }
}
