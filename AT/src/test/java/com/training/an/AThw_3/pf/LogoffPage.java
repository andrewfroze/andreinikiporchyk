package com.training.an.AThw_3.pf;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogoffPage extends Page {

    @FindBy(xpath = "//h1[@class='ahT6S ']")
    private WebElement logoffPageSign;

    public LogoffPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getLogoffPageSign() {
        waitForElementVisibility(logoffPageSign);
        return logoffPageSign;
    }
}
