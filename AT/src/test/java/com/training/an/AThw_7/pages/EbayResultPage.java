package com.training.an.AThw_7.pages;

import org.openqa.selenium.By;

public class EbayResultPage extends AbstractPage {
    private final By listOfProducts = By.xpath("//*[@id='mainContent']//li[contains(@class,'s-item')]//h3/..");

    public EbayProductPage clickOnFirstProduct() {
        driver.findElements(listOfProducts).get(0).click();
        return new EbayProductPage();
    }

    public String getTextFromFirstProduct() {
        return driver.findElements(listOfProducts).get(0).getText();
    }

}
