package com.training.an.AThw_7.pages;

import com.training.an.AThw_7.utils.waitUtils.WaitUtils;
import org.openqa.selenium.By;

public class EbayHomePage extends AbstractPage {
    private static final String BASE_URL = "https://www.ebay.com/";
    private final By searchInput = By.xpath("//input[@type='text']");
    private final By submit = By.xpath("//input[@type='submit']");

    public EbayHomePage open() {
        driver.get(BASE_URL);
        return this;
    }

    public EbayHomePage enterQuery(String query) {
        WaitUtils.waitForElementVisible(driver, searchInput);
        driver.findElement(searchInput).sendKeys(query);
        return new EbayHomePage();
    }

    public EbayResultPage clickSubmit() {
        WaitUtils.waitForElementVisible(driver, submit);
        driver.findElement(submit).click();
        return new EbayResultPage();
    }
}
