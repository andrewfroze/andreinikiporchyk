package com.training.an.AThw_7.pages;

import com.training.an.AThw_7.utils.waitUtils.WaitUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.List;

public class EbayProductPage extends AbstractPage {
    private final By addToCart = By.xpath("//a[@id='isCartBtn_btn']");
    private final By itemTitle = By.xpath("//h1[@id='itemTitle']");
    private final By selectFieldsLocator = By.xpath("//select");
    private final By selectFieldsFirstOption = By.xpath("//select/option[2]");

    public String getSelectedProductFullName() {
        return driver.findElement(itemTitle).getText();
    }

    public EbayProductPage selectFirstOptionInAllSelectFields() {
        List<WebElement> selectFields = driver.findElements(selectFieldsLocator);
        if (selectFields.size() > 2) {
            List<WebElement> firstOption = driver.findElements(selectFieldsFirstOption);
            for (int i = 1, l = selectFields.size(); i < l - 1; i++) {
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", selectFields.get(i));
                selectFields.get(i).click();
                for (int j = 1; j < l - 1; j++) {
                    if (firstOption.get(j).isDisplayed()) {
                        firstOption.get(i).click();
                        firstOption = driver.findElements(selectFieldsFirstOption);
                        break;
                    }
                }
            }
        }
        return new EbayProductPage();
    }

    public EbayCartPage addProductToCart() {
        WaitUtils.waitForElementVisible(driver, addToCart);
        driver.findElement(addToCart).click();
        return new EbayCartPage();
    }
}