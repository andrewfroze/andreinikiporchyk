package com.training.an.AThw_7.pages;

import org.openqa.selenium.By;

public class EbayCartPage extends AbstractPage {
    private static final By itemInCart = By.xpath("//*[@class='cart-bucket']//h3//span[@class='BOLD']");
    private static final By numberOfItems = By.xpath("//header//li[@id='gh-minicart-hover']//i");

    public String getLastAddedItemName() {
        return driver.findElement(itemInCart).getText();
    }

    public int getNumberOfAddedItems() {
        return Integer.parseInt(driver.findElement(numberOfItems).getText());
    }
}
