package com.training.an.AThw_7.steps;

import com.training.an.AThw_7.browser.Browser;
import com.training.an.AThw_7.pages.EbayCartPage;
import com.training.an.AThw_7.pages.EbayHomePage;
import com.training.an.AThw_7.pages.EbayProductPage;
import com.training.an.AThw_7.pages.EbayResultPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import static org.hamcrest.CoreMatchers.containsString;


public class EbayStepDefs {
    private static String productNameForAssertion;
    private static final String cartURL = "https://cart.payments.ebay.com/";
    private static EbayProductPage ebayProductPage;

    @Given("^I opened Ebay Page$")
    public void iOpenedEbayPage() {
        new EbayHomePage().open();
    }

    @When("^I search the product \"([^\"]*)\"$")
    public void iSearchTheProduct(String query) {
        new EbayHomePage().enterQuery(query).clickSubmit();
    }

    @Then("^the term query \"([^\"]*)\" should be the first in the Search Result grid$")
    public void theTermQueryShouldBeTheFirstInTheSearchResultGrid(String expectPhrase) {
        String firstProductName = new EbayResultPage().getTextFromFirstProduct().toLowerCase();

        Assert.assertThat(firstProductName, containsString(expectPhrase.toLowerCase()));
    }

    @When("^I open first product's page$")
    public void iOpenFirstProductSPage() {
        ebayProductPage = new EbayResultPage().clickOnFirstProduct().selectFirstOptionInAllSelectFields();
        productNameForAssertion = ebayProductPage.getSelectedProductFullName();
    }

    @When("^I add opened product to the cart$")
    public void iAddOpenedProductToTheCart() {
        ebayProductPage.addProductToCart();
    }

    @Then("^the Cart Page should be opened$")
    public void theCartPageShouldBeOpened() {
        String actualURL = Browser.getWebDriverInstance().getCurrentUrl();

        Assert.assertEquals(actualURL, cartURL);
    }

    @Then("^the name of added product should be the same, like the name on previous product page$")
    public void theNameOfAddedProductShouldBeTheSameLikeTheNameOnPreviousProductPage() {
        String lastItemInCartName = new EbayCartPage().getLastAddedItemName();

        Assert.assertThat(productNameForAssertion, containsString(lastItemInCartName));
    }

    @Then("^the number \"([^\"]*)\" is situated above the Cart icon$")
    public void theNumberIsSituatedAboveTheCartIcon(String itemsInCart) {
        int actualItemsInCart = new EbayCartPage().getNumberOfAddedItems();

        Assert.assertEquals(actualItemsInCart, Integer.parseInt(itemsInCart));
    }
}
