package com.training.an.AThw_7;

import com.training.an.AThw_7.browser.Browser;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {
                "pretty",
                "json:target/Cucumber.json",
                "html:target/cucumber-html-report"
        }, tags = {"@smoke"}
)
public class RunAcceptanceTest {

    @AfterClass
    public static void closeDriver() {
        Browser.close();
    }
}
