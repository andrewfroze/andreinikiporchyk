@smoke @search
Feature: Search product

  Scenario:Running a Full Text Quick Search
    Given I opened Ebay Page
    When I search the product "Xiaomi"
    Then the term query "Xiaomi" should be the first in the Search Result grid

  Scenario Outline: Running a Full Text Quick Search
    Given I opened Ebay Page
    When I search the product "<request>"
    Then the term query "<request>" should be the first in the Search Result grid

    Examples:
      | request |
      | iPhone  |
      | Xiaomi  |
      | Samsung |