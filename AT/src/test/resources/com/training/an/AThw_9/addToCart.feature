@smoke @addToCart
Feature: Add to Cart

  Scenario: Adding first founded product to the cart
    Given I opened Ebay Page
    When I search the product "iPhone"
    Then the term query "iPhone" should be the first in the Search Result grid
    When I open first product's page
    And I add opened product to the cart
    Then the Cart Page should be opened
    And the name of added product should be the same, like the name on previous product page
    And the number "1" is situated above the Cart icon