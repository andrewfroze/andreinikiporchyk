package com.training.an.hw_4.task1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AtmTest {
    Card debitCard;
    Card creditCard;
    Atm atm;

    @Before
    public void cardCreation() {
        debitCard = new DebitCard("userName", 450.1);
        creditCard = new CreditCard("userName", 450.1);
        atm = new Atm();
    }


    @Test
    public void testDecreaseBalanceInCreditCard() {
        atm.decreaseBalance(creditCard, 18.2);
        Assert.assertEquals(431.9, creditCard.getBalance(), 0.000001);
    }

    @Test
    public void testDecreaseBalanceToNegativeInCreditCard() {
        atm.decreaseBalance(creditCard, 550.1);
        Assert.assertEquals(-100, creditCard.getBalance(), 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDecreaseBalanceInCreditCardNegative() {
        atm.decreaseBalance(creditCard, -550.1);
        Assert.assertEquals(450.1, creditCard.getBalance(), 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDecreaseBalanceInCreditCardNegative1() {
        atm.decreaseBalance(creditCard, 0);
        Assert.assertEquals(450.1, creditCard.getBalance(), 0.000001);
    }

    @Test
    public void testDecreaseBalanceInDebitCard() {
        atm.decreaseBalance(debitCard, 50.1);
        Assert.assertEquals(400, debitCard.getBalance(), 0.000001);
    }

    @Test(expected = DecreasingBalanceException.class)
    public void testDecreaseBalanceToNegativeInDebitCard() {
        atm.decreaseBalance(debitCard, 550.1);
        Assert.assertEquals(450.1, debitCard.getBalance(), 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDecreaseBalanceInDebitCardNegative() {
        atm.decreaseBalance(debitCard, -1);
        Assert.assertEquals(450.1, debitCard.getBalance(), 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDecreaseBalanceInDebitCardNegative1() {
        atm.decreaseBalance(debitCard, 0);
        Assert.assertEquals(450.1, debitCard.getBalance(), 0.000001);
    }

    @Test
    public void testIncreaseBalanceInCreditCard() {
        atm.increaseBalance(creditCard, 18.2);
        Assert.assertEquals(468.3, creditCard.getBalance(), 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncreaseBalanceInCreditCardNegative() {
        atm.increaseBalance(creditCard, -2);
        Assert.assertEquals(450.1, creditCard.getBalance(), 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncreaseBalanceInCreditCardNegative1() {
        atm.increaseBalance(creditCard, 0);
        Assert.assertEquals(450.1, creditCard.getBalance(), 0.000001);
    }

    @Test
    public void testIncreaseBalanceInDebitCard() {
        atm.increaseBalance(debitCard, 18.1);
        Assert.assertEquals(468.2, debitCard.getBalance(), 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncreaseBalanceInDebitCardNegative() {
        atm.increaseBalance(debitCard, -2);
        Assert.assertEquals(450.1, debitCard.getBalance(), 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncreaseBalanceInDebitCardNegative1() {
        atm.increaseBalance(debitCard, 0);
        Assert.assertEquals(450.1, debitCard.getBalance(), 0.000001);
    }
}
