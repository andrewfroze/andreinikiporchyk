package com.training.an.hw_3;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CardTest {
    private Card card;

    @Before
    public void cardCreation() {
        card = new Card("userName", 450.1);
    }

    @Test
    public void testIncreaseBalancePositive() {
        Assert.assertTrue(card.increaseBalance(50.55));
        Assert.assertEquals(500.65, card.getBalance(), 0.000001);
        Assert.assertTrue(card.increaseBalance(0.01));
        Assert.assertEquals(500.66, card.getBalance(), 0.000001);
    }

    @Test
    public void testIncreaseBalanceNegative() {
        Assert.assertFalse(card.increaseBalance(-50.65));
        Assert.assertEquals(450.1, card.getBalance(), 0.000001);
        Assert.assertFalse(card.increaseBalance(0));
        Assert.assertEquals(450.1, card.getBalance(), 0.000001);
    }

    @Test
    public void testDecreaseBalancePositive() {
        Assert.assertTrue(card.decreaseBalance(18.2));
        Assert.assertEquals(431.9, card.getBalance(), 0.000001);
        Assert.assertTrue(card.decreaseBalance(0.01));
        Assert.assertEquals(431.89, card.getBalance(), 0.000001);
    }

    @Test
    public void testDecreaseBalanceNegative() {
        Assert.assertFalse(card.decreaseBalance(-1.2));
        Assert.assertEquals(450.1, card.getBalance(), 0.000001);
        Assert.assertFalse(card.decreaseBalance(0));
        Assert.assertEquals(450.1, card.getBalance(), 0.000001);
    }

    @Test
    public void testGetBalanceInAnotherCurrencyPositive() {
        double balance = card.getBalanceInAnotherCurrency(2.2);
        Assert.assertEquals(990.22, balance, 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetBalanceInAnotherCurrencyNegative() {
        double balance = card.getBalanceInAnotherCurrency(-2.2);
        Assert.assertEquals(450.1, balance, 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetBalanceInAnotherCurrencyNegativeWithZero() {
        double balance = card.getBalanceInAnotherCurrency(0);
        Assert.assertEquals(450.1, balance, 0.000001);
    }
}
