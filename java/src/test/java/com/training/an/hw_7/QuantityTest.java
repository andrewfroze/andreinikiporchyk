package com.training.an.hw_7;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class QuantityTest {
    private Quantity<Integer> quantity1;
    private Quantity<Integer> quantity2;
    private final int[] dataArrayForQantity1 = {1, 2, 3, 4, 5};
    private final int[] dataArrayForQantity2 = {3, 4, 5, 6, 7};

    @Before
    public void dataGeneration() {
        quantity1 = new Quantity<Integer>();
        quantity2 = new Quantity<Integer>();
        for (int i = 0; i < 5; i++) {
            quantity1.add(dataArrayForQantity1[i]);
            quantity2.add(dataArrayForQantity2[i]);
        }
    }

    @Test
    public void testIntersection() {
        Quantity<Integer> intersection = quantity1.intersection(quantity2);
        Assert.assertTrue(intersection.remove(3));
        Assert.assertTrue(intersection.remove(4));
        Assert.assertTrue(intersection.remove(5));
        Assert.assertTrue(intersection.isEmpty());
    }

    @Test
    public void testUnion() {
        Quantity<Integer> union = quantity1.union(quantity2);
        Assert.assertTrue(union.remove(1));
        Assert.assertTrue(union.remove(2));
        Assert.assertTrue(union.remove(3));
        Assert.assertTrue(union.remove(4));
        Assert.assertTrue(union.remove(5));
        Assert.assertTrue(union.remove(6));
        Assert.assertTrue(union.remove(7));
        Assert.assertTrue(union.isEmpty());
    }

    @Test
    public void testDifference() {
        Quantity<Integer> difference = quantity1.difference(quantity2);
        Assert.assertTrue(difference.remove(1));
        Assert.assertTrue(difference.remove(2));
        Assert.assertTrue(difference.isEmpty());
    }

    @Test
    public void testSymmetricDifference() {
        Quantity<Integer> symmetricDifference = quantity1.symmetricDifference(quantity2);
        Assert.assertTrue(symmetricDifference.remove(1));
        Assert.assertTrue(symmetricDifference.remove(2));
        Assert.assertTrue(symmetricDifference.remove(6));
        Assert.assertTrue(symmetricDifference.remove(7));
        Assert.assertTrue(symmetricDifference.isEmpty());
    }
}
