package com.training.an.hw_8;

import java.io.*;
import java.util.ArrayList;

public class EmployeesDatabase {
    private String filePath;
    private int maxId;
    ArrayList<Employee> employees = new ArrayList<Employee>();

    public EmployeesDatabase(String filePath) {
        this.filePath = filePath;
        BufferedReader bufferedReader = null;
        try {
            File employeesDataBase = new File(filePath);
            bufferedReader = new BufferedReader(new FileReader(employeesDataBase));
            String line;
            maxId = 0;
            String[] splitLine;
            while ((line = bufferedReader.readLine()) != null && !line.matches("\\s*")) {
                splitLine = line.split("\\.?\\s");
                employees.add(new Employee(Integer.parseInt(splitLine[0]), splitLine[1], splitLine[2]));
                if (Integer.parseInt(splitLine[0]) > maxId) {
                    maxId = Integer.parseInt(splitLine[0]);
                }
            }
        } catch (FileNotFoundException ex) {
            createNewDB(filePath);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void createNewDB(String filePath) {
        try {
            File newFile = new File(filePath);
            newFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean addEmployee(String firstName, String secondName) {
        try {
            employees.add(new Employee(maxId + 1, firstName, secondName));
            maxId += 1;
            System.out.println("Employee added successfully");
            return true;
        } catch (Exception e) {
            System.out.println("Employee hasn't added");
            return false;
        }
    }

    public boolean removeEmployee(String idToRemove) {
        boolean result = false;
        try {
            for (Employee employee : employees) {
                if (employee.getId() == Integer.parseInt(idToRemove)) {
                    employees.remove(employee);
                    result = true;
                    System.out.println("Employee removed successfully");
                    break;
                }
            }
            if (!result) {
                System.out.println("There isn't such Id");
            }
        } catch (NumberFormatException e) {
            System.out.println("Id for remove must be numeric");
        }
        return result;
    }

    public void close() {
        try {
            File fileToWrite = new File(filePath);
            FileWriter fileWriter = new FileWriter(fileToWrite, false);
            for (Employee employee : employees) {
                fileWriter.write(employee.toString() + "\n");
            }
            fileWriter.flush();
            fileWriter.close();
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        String result = "";
        for (Employee employee : employees) {
            result += employee.toString() + "\n";
        }
        return result;
    }

    public String getFilePath() {
        return filePath;
    }

    public int getMaxId() {
        return maxId;
    }
}
