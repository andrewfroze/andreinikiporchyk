package com.training.an.hw_8;

import java.util.Scanner;

public class Main {
    private static final String databaseAddress = "./src/main/resources/employees.txt";

    public static void main(String[] args) {
        EmployeesDatabase employeesDatabase = new EmployeesDatabase(databaseAddress);
        System.out.println(employeesDatabase);
        System.out.println("For help enter \"help\"");
        Scanner scanCommand = new Scanner(System.in);

        while (true) {
            String[] command = scanCommand.nextLine().split("\\s");
            switch (command[0]) {
                case "add": {
                    addEmployee(employeesDatabase, command);
                    break;
                }
                case "remove": {
                    removeEmployee(employeesDatabase, command[1]);
                    break;
                }
                case "close": {
                    employeesDatabase.close();
                    break;
                }
                case "print": {
                    System.out.println(employeesDatabase);
                    break;
                }
                case "help": {
                    System.out.println("Main commands:\nAdd new employee           - add firstName secondName\nRemove employee by Id      - remove idToRemove \nSave and close             - close \nShow all employee database - print ");
                    break;
                }
                default: {
                    System.out.println("There isn't such command");
                }
            }
        }
    }

    private static void removeEmployee(EmployeesDatabase employeesDatabase, String idToRemove) {
        try {
            employeesDatabase.removeEmployee(idToRemove);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Command looks like: remove idToRemove");
        }
    }

    private static void addEmployee(EmployeesDatabase employeesDatabase, String[] command) {
        try {
            employeesDatabase.addEmployee(command[1], command[2]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Command looks like: add firstName secondName");
        }
    }
}
