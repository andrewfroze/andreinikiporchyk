package com.training.an.hw_1;

public class Task2 {


    public static void main(String[] args) {
        int algorithmId = Integer.parseInt(args[0]);
        int loopType = Integer.parseInt(args[1]);
        int n = Integer.parseInt(args[2]);

        switch (algorithmId) {
            case 1: {
                if (n > 0) {
                    printFibonacci(n, loopType);
                } else {
                    System.out.println("Unsuitable algorithm parameter");
                }
                break;
            }
            case 2: {
                printFactorial(n, loopType);
                break;
            }
            default: {
                System.out.println("Undeclared algorithm ID");
                break;
            }
        }
    }

    public static void printFibonacci(int n, int loopType) {
        switch (loopType) {
            case 1: {
                fibonacciByWhile(n);
                break;
            }
            case 2: {
                fibonacciByDoWhile(n);
                break;
            }
            case 3: {
                fibonacciByFor(n);
                break;
            }
            default: {
                System.out.println("Undeclared loop type");
                break;
            }
        }
    }


    public static void printFactorial(int n, int loopType) {
        switch (loopType) {
            case 1: {
                factorialByWhile(n);
                break;
            }
            case 2: {
                factorialByDoWhile(n);
                break;
            }
            case 3: {
                factorialByFor(n);
                break;
            }
            default: {
                System.out.println("Undeclared loop type");
                break;
            }
        }
    }

    public static void fibonacciByWhile(int n) {
        if (n > 1) {
            int[] fibonacсi = new int[n];
            fibonacсi[0] = 0;
            fibonacсi[1] = 1;
            System.out.print("First " + n + " fibonacci numbers: " + fibonacсi[0] + " " + fibonacсi[1]);
            int i = 2;
            while (i < n) {
                fibonacсi[i] = fibonacсi[i - 1] + fibonacсi[i - 2];
                System.out.print(" " + fibonacсi[i]);
                i++;
            }
        } else if (n < 2) {
            int[] fibonacсi = new int[1];
            fibonacсi[0] = 0;
            switch (n) {
                case 1: {
                    System.out.println("First fibonacci number: " + fibonacсi[0]);
                    break;
                }
            }
        }
    }

    public static void fibonacciByFor(int n) {
        if (n > 1) {
            int[] fibonacсi = new int[n];
            fibonacсi[0] = 0;
            fibonacсi[1] = 1;
            System.out.print("First " + n + " fibonacci numbers: " + fibonacсi[0] + " " + fibonacсi[1]);

            for (int i = 2; i < n; i++) {
                fibonacсi[i] = fibonacсi[i - 1] + fibonacсi[i - 2];
                System.out.print(" " + fibonacсi[i]);
            }
        } else if (n < 2) {
            int[] fibonacсi = new int[1];
            fibonacсi[0] = 0;
            switch (n) {
                case 1: {
                    System.out.println("First fibonacci number: " + fibonacсi[0]);
                    break;
                }
            }
        }
    }

    public static void fibonacciByDoWhile(int n) {
        if (n > 1) {
            int[] fibonacсi = new int[n];
            fibonacсi[0] = 0;
            fibonacсi[1] = 1;
            System.out.print("First " + n + " fibonacci numbers: " + fibonacсi[0] + " " + fibonacсi[1]);
            int i = 2;
            do {
                if (n > 2) {
                    fibonacсi[i] = fibonacсi[i - 1] + fibonacсi[i - 2];
                    System.out.print(" " + fibonacсi[i]);
                    i++;
                }
            } while (i < n);

        } else if (n < 2) {
            int[] fibonacсi = new int[1];
            fibonacсi[0] = 0;
            switch (n) {
                case 1: {
                    System.out.println("First fibonacci number: " + fibonacсi[0]);
                    break;
                }
            }
        }
    }

    public static void factorialByWhile(int n) {
        int factorial = 1;
        if (n > -1) {
            int i = 1;
            while (i < n) {
                i++;
                factorial *= i;
            }
            System.out.println(n + "! = " + factorial);
        } else {
            System.out.println("Unsuitable algorithm parameter");
        }
    }

    public static void factorialByDoWhile(int n) {
        int factorial = 1;
        if (n > -1) {
            int i = 1;
            do {
                factorial *= i;
                i++;
            } while (i < n + 1);
            System.out.println(n + "! = " + factorial);

        } else {
            System.out.println("Unsuitable algorithm parameter");
        }
    }

    public static void factorialByFor(int n) {
        int factorial = 1;
        if (n > -1) {
            for (int i = 0; i < n; i++) {
                factorial *= (i + 1);
            }
            System.out.println(n + "! = " + factorial);
        } else {
            System.out.println("Unsuitable algorithm parameter");
        }
    }

}
