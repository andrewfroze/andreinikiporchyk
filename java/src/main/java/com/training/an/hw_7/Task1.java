package com.training.an.hw_7;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Task1 {

    public static void main(String[] args) {
        Integer[] array = new Integer[]{75, 1223, 894, 1, 44, 91923, 65, 24, 5522};
        List<Integer> arrayList = Arrays.asList(array);
        Collections.sort(arrayList, new NumberComparator());
        System.out.println(arrayList.toString());
    }
}