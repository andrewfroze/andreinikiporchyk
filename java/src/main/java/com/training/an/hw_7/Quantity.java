package com.training.an.hw_7;

import java.util.HashSet;

public class Quantity<T> extends HashSet<T> {

    public Quantity() {
        super();
    }

    public Quantity(Quantity<T> value) {
        super(value);
    }

    public Quantity<T> intersection(Quantity<T> anotherQuantity) {
        Quantity<T> result = new Quantity<T>();
        for (T element : this) {
            if (anotherQuantity.contains(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public Quantity<T> union(Quantity<T> anotherQuantity) {
        Quantity<T> result = new Quantity<T>(anotherQuantity);
        for (T element : this) {
            if (!anotherQuantity.contains(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public Quantity<T> difference(Quantity<T> anotherQuantity) {
        Quantity<T> result = new Quantity<T>(this);
        for (T element : anotherQuantity) {
            if (this.contains(element)) {
                result.remove(element);
            }
        }
        return result;
    }

    public Quantity<T> symmetricDifference(Quantity<T> anotherQuantity) {
        Quantity<T> result = this.union(anotherQuantity).difference(this.intersection(anotherQuantity));
        return result;
    }

}
