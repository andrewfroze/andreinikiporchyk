package com.training.an.hw_7;

import java.io.Serializable;
import java.util.Comparator;

public class NumberComparator implements Comparator, Serializable {
    public int compare(Object o1, Object o2) {
        Integer SumA = 0;
        Integer SumB = 0;
        for (int i = 0, l = String.valueOf(o1).length(); i < l; i++) {
            SumA += Character.getNumericValue(String.valueOf(o1).charAt(i));
        }
        for (int i = 0, l = String.valueOf(o2).length(); i < l; i++) {
            SumB += Character.getNumericValue(String.valueOf(o2).charAt(i));
        }
        return SumA.compareTo(SumB);
    }

}
