package com.training.an.hw_4.task1;

public class DecreasingBalanceException extends IllegalArgumentException {
    public DecreasingBalanceException(String message) {
        super(message);
    }
}
