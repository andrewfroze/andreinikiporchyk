package com.training.an.hw_4.task2;

public class SelectionSort extends Sorter implements SortingStrategy {
    public int[] execute(int[] array) {
        for (int i = 0, l = array.length; i < l; i++) {
            int jMin = i;
            for (int j = i + 1; j < l; j++) {
                if (array[j] < array[jMin]) {
                    jMin = j;
                }
            }

            if (jMin != i) {
                array[i] += array[jMin];
                array[jMin] = array[i] - array[jMin];
                array[i] -= array[jMin];
            }
        }
        return array;
    }
}
