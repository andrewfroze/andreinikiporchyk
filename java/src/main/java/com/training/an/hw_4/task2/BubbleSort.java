package com.training.an.hw_4.task2;

public class BubbleSort extends Sorter implements SortingStrategy {

    public int[] execute(int[] array) {
        boolean sort = true;
        while (sort) {
            sort = false;
            for (int i = 0, l = array.length - 1; i < l; i++) {
                if (array[i] > array[i + 1]) {
                    sort = true;
                    array[i] += array[i + 1];
                    array[i + 1] = array[i] - array[i + 1];
                    array[i] -= array[i + 1];
                }
            }
        }
        return array;
    }
}
