package com.training.an.hw_4.task2;

public interface SortingStrategy {
    int[] execute(int[] array);
}
