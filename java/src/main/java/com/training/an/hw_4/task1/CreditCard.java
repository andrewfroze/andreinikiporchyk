package com.training.an.hw_4.task1;

public class CreditCard extends Card {

    public CreditCard(String ownerName, double balance) {
        super(ownerName, balance);
    }

    public CreditCard(String ownerName) {
        super(ownerName);
    }
}
