package com.training.an.hw_4.task1;

public class Atm {

    public void increaseBalance(Card card, double sum) {
        card.increaseBalance(sum);
    }

    public void decreaseBalance(Card card, double sum) throws com.training.an.hw_4.task1.DecreasingBalanceException {
        card.decreaseBalance(sum);
    }
}
