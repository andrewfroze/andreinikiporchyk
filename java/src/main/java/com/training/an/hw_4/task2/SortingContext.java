package com.training.an.hw_4.task2;

public class SortingContext {
    private SortingStrategy strategy;

    public SortingContext(Sorter sortStrategy) {
        this.strategy = sortStrategy;
    }

    public void setStrategy(SortingStrategy strategy) {
        this.strategy = strategy;
    }

    public int[] executeStrategy(int[] array) {
        return strategy.execute(array);
    }
}
