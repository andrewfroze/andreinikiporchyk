package com.training.an.hw_4.task1;

public class DebitCard extends Card {

    public DebitCard(String ownerName, double balance) {
        super(ownerName, balance);
    }

    public DebitCard(String ownerName) {
        super(ownerName);
    }

    public void decreaseBalance(double sum) throws DecreasingBalanceException {
        if (sum > 0) {
            if (super.getBalance() >= sum) {
                super.decreaseBalance(sum);
            } else {
                throw new DecreasingBalanceException("Too low balance");
            }
        } else {
            throw new IllegalArgumentException("Decrease sum must be positive");
        }
    }
}

