package com.training.an.hw_4.task1;


public class Card {
    private String ownerName;
    private double balance;

    public Card(String ownerName, double balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    public Card(String ownerName) {
        this.ownerName = ownerName;
    }

    public void increaseBalance(double sum) {
        if (sum > 0) {
            this.balance += sum;
        } else {
            throw new IllegalArgumentException ("Decrease sum must be positive");
        }
    }

    public void decreaseBalance(double sum) {
        if (sum > 0) {
            this.balance -= sum;
        } else {
            throw new IllegalArgumentException ("Decrease sum must be positive");
        }
    }

    public double getBalance() {
        return balance;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public double getBalanceInAnotherCurrency(double conversionRate) {
        if (conversionRate > 0) {
            return balance * conversionRate;
        } else {
            throw new IllegalArgumentException("Conversion rate must be positive");
        }
    }
}
