package com.training.an.hw_4.task2;

import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int[] array = new int[20];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }
        System.out.println("Unsorted:  " + Arrays.toString(array));

        int strategyIndex;
        do {
            System.out.println("Choose sorting strategy: \n1 - Bubble Sort \n2 - Selection Sort ");
            Scanner scanner = new Scanner(System.in);
            strategyIndex = scanner.nextInt();
        } while (strategyIndex != 1 && strategyIndex != 2);

        Sorter sortStrategy = null;
        switch (strategyIndex) {
            case 1: {
                sortStrategy = new BubbleSort();
                break;
            }
            case 2: {
                sortStrategy = new SelectionSort();
                break;
            }
            default: {
                System.out.println("There isn't such strategy");
            }
        }
        System.out.println("Unsorted:  " + Arrays.toString(array));
        SortingContext sortingContext = new SortingContext(sortStrategy);
        sortingContext.executeStrategy(array);
        System.out.println("Sorted:  " + Arrays.toString(array));
    }
}
