package com.training.an.hw_6;

class ArrayWrapper<T> {
    private T[] a;

    public ArrayWrapper(T[] a) {
        this.a = a;
    }

    public T get(int i) throws IncorrectArrayWrapperIndex {
        try {
            return a[i - 1];
        } catch (IndexOutOfBoundsException e) {
            throw new IncorrectArrayWrapperIndex("incorrect array index");
        }
    }

    public boolean replace(int i, T replaceValue) {
        if (replaceValue instanceof String) {
            if (String.valueOf(replaceValue).length() > String.valueOf(a[i - 1]).length()) {
                a[i - 1] = replaceValue;
                return true;
            }
        } else if (replaceValue instanceof Integer) {
            if ((Integer) replaceValue > (Integer) a[i - 1]) {
                a[i - 1] = replaceValue;
                return true;
            }
        }

        return false;
    }

}
