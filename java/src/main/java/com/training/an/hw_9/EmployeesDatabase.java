package com.training.an.hw_9;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.DriverManager;
import java.sql.SQLException;

public class EmployeesDatabase {
    private static String filePath;
    private static int maxId;

    public static void setFilePath(String setFilePath) {
        filePath = setFilePath;
    }

    public static boolean addEmployee(String firstName, String lastName) {
        boolean result = true;
        try {
            Class.forName("org.sqlite.JDBC");
            Connection connection = null;
            connection = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            maxId += 1;
            PreparedStatement preparedStatement = connection.prepareStatement("insert into Employees values  (" + maxId + ", " + "\"" + firstName + "\", \"" + lastName + "\")");
            preparedStatement.execute();
            preparedStatement.close();
            connection.close();
            System.out.println("Employee added successfully");
        } catch (SQLException e) {
            result = false;
            System.out.println("Employee hasn't added");
            System.out.println("There is not connection to DB");
        } catch (ClassNotFoundException foundException) {
            System.out.println("Employee hasn't added");
            foundException.printStackTrace();
        }
        return result;
    }

    public static boolean removeEmployee(int idToRemove) {
        boolean result = true;
        try {
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            PreparedStatement checking = connection.prepareStatement("select * from Employees where id = " + idToRemove);
            ResultSet resultSet = checking.executeQuery();
            if (!resultSet.next()) {
                checking.close();
                resultSet.close();
                connection.close();
                result = false;
                System.out.println("There isn't employee with id = " + idToRemove);
            } else {
                PreparedStatement preparedStatement = connection.prepareStatement("delete from Employees where  id = " + String.valueOf(idToRemove));
                preparedStatement.execute();
                preparedStatement.close();
                connection.close();
                System.out.println("Employee removed successfully");
            }
        } catch (SQLException e) {
            System.out.println("There is not connection to BD");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (idToRemove == maxId) {
            getMaxId();
        }
        return result;
    }

    public static void close() {
        System.exit(0);
    }

    public static void printDatabase() {
        String result = "";
        try {
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            PreparedStatement preparedStatement = connection.prepareStatement("select * from Employees");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String firstName = resultSet.getString("firstname");
                String lastName = resultSet.getString("lastname");
                result += id + ". " + firstName + " " + lastName + "\n";
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("There is not connection to BD");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(result);
    }


    public static void getMaxId() {
        maxId = 0;
        try {
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + filePath);
            PreparedStatement preparedStatement = connection.prepareStatement("select * from Employees");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                if (id > maxId) {
                    maxId = id;
                }
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("There is not connection to BD");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
