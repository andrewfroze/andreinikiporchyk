package com.training.an.hw_9;

import java.util.Scanner;

public class Main {
    private static final String dataBaseFilePath = "src/main/resources/db/employees.db";


    public static void main(String[] args) {
        EmployeesDatabase.setFilePath(dataBaseFilePath);
        EmployeesDatabase.getMaxId();
        EmployeesDatabase.printDatabase();
        System.out.println("For help enter \"help\"");
        Scanner scanCommand = new Scanner(System.in);

        while (true) {
            String[] command = scanCommand.nextLine().split("\\s");
            switch (command[0]) {
                case "add": {
                    addEmployee(command);
                    break;
                }
                case "remove": {
                    removeEmployee(command[1]);
                    break;
                }
                case "close": {
                    EmployeesDatabase.close();
                    break;
                }
                case "print": {
                    EmployeesDatabase.printDatabase();
                    break;
                }
                case "help": {
                    System.out.println("Main commands:\nAdd new employee           - add firstName lastName\nRemove employee by Id      - remove idToRemove \nSave and close             - close \nShow all employee database - print ");
                    break;
                }
                default: {
                    System.out.println("There isn't such command");
                }
            }
        }
    }

    private static void removeEmployee(String s) {
        try {
            EmployeesDatabase.removeEmployee(Integer.parseInt(s));
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Command looks like: remove idToRemove");
        }
    }

    private static void addEmployee(String[] command) {
        try {
            EmployeesDatabase.addEmployee(command[1], command[2]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Command looks like: add firstName secondName");
        }
    }
}
