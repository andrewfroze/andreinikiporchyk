package com.training.an.hw_5;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        String inputString = "Once upon a time a Wolf was lapping at a spring on a hillside, when, looking up, what should he see but a Lamb just beginning to drink a little lower down.";
        inputString = inputString.replaceAll(",|\\.|\\d|\\W", " ");
        inputString = inputString.toLowerCase();
        String[] words = inputString.split("\\s+");
        Arrays.sort(words);
        System.out.print(Character.toUpperCase(words[0].charAt(0)) + ": ");
        System.out.print(words[0]);
        for (int i = 1, l = words.length; i < l; i++) {
            if (!words[i].equals(words[i - 1])) {
                if (words[i].charAt(0) == words[i - 1].charAt(0)) {
                    System.out.print(" " + words[i]);
                } else {
                    System.out.print("\n" + Character.toUpperCase(words[i].charAt(0)) + ": ");
                    System.out.print(words[i]);
                }
            }
        }
    }
}
