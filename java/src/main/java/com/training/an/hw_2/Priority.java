package com.training.an.hw_2;

public class Priority {
    static int statValue = 11;
    int simpleClassValue = 10;

    {
        if (simpleClassValue == 10) {
            System.out.println("simple class variable initialization");
        }
        int firstBlockValue = 1;
        int secondBlockValue = 2;
        int thirdBlockValue = 3;
        System.out.println("simple block #1");
    }

    {
        int fourthBlockValue = 4;
        int fifthBlockValue = 5;
        int sixthBlockValue = 6;
        System.out.println("simple block #2");
    }

    static {
        if (statValue == 11) {
            System.out.println("static class variable initialization");
        }
        int firstStaticBlockValue = 1;
        int secondStaticBlockValue = 2;
        int thirdStaticBlockValue = 3;
        System.out.println("static block #1");
    }

    static {
        int fourthStaticBlockValue = 4;
        int fifthStaticBlockValue = 5;
        int sixthStaticBlockValue = 6;
        System.out.println("static block #2");
    }


    public static void main(String[] args) {
        Priority priority;
        System.out.println("creation of object");
        priority = new Priority();
        System.out.println("assigning a reference to the object");
    }
}
