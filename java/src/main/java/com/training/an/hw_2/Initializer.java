package com.training.an.hw_2;

public class Initializer {
    private static int i;
    private static byte b;
    private static long l;
    private static float f;
    private static double d;
    private static char ch;
    private static boolean bo;
    private static short sh;

    private static Integer in;

    public static void main(String[] args) {
        System.out.println("Default values for primitive types:");
        System.out.println("\tbyte: " + b);
        System.out.println("\tshort: " + sh);
        System.out.println("\tint: " + i);
        System.out.println("\tlong: " + l);
        System.out.println("\tfloat: " + f);
        System.out.println("\tdouble: " + d);
        System.out.println("\tchar: " + ch);
        System.out.println("\tboolean: " + bo);
        System.out.println("\nDefault value for reference types: " + in);
    }
}
