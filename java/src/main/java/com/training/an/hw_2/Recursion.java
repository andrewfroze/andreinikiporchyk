package com.training.an.hw_2;

public class Recursion {
    private static int counterInt = 2;
    private static int counterLong = 2;

    public static void main(String[] args) {
        int a = 0;
        int b = 1;
        long al = 0L;
        long bl = 1L;
        fibonacciCounter(a, b);
        System.out.println("Max fibinacci number for int is on " + counterInt + " position");
        fibonacciCounter(al, bl);
        System.out.println("Max fibinacci number for long is on " + counterLong + " position");


    }

    public static void fibonacciCounter(int firstNumber, int secondNumber) {
        firstNumber += secondNumber;
        if (firstNumber > 0) {
            counterInt++;
            fibonacciCounter(secondNumber, firstNumber);
        }
    }

    public static void fibonacciCounter(long firstNumber, long secondNumber) {
        firstNumber += secondNumber;
        if (firstNumber > 0) {
            counterLong++;
            fibonacciCounter(secondNumber, firstNumber);
        }
    }
}
