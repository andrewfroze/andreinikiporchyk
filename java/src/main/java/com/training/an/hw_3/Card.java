package com.training.an.hw_3;


public class Card {
    private String ownerName;
    private double balance;

    public Card(String ownerName, double balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    public Card(String ownerName) {
        this.ownerName = ownerName;
    }

    public boolean increaseBalance(double sum) {
        if (sum > 0) {
            this.balance += sum;
            return true;
        } else {
            System.out.println("Increase sum must be positive");
            return false;
        }
    }

    public boolean decreaseBalance(double sum) {
        if (sum > 0) {
            this.balance -= sum;
            return true;
        } else {
            System.out.println("Decrease sum must be positive");
            return false;
        }
    }

    public double getBalance() {
        return balance;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public double getBalanceInAnotherCurrency(double conversionRate) {
        if (conversionRate > 0) {
            return balance * conversionRate;
        } else {
            throw new IllegalArgumentException("Conversion rate must be positive");
        }
    }
}
