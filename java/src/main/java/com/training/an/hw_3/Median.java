package com.training.an.hw_3;

import java.util.Arrays;

public abstract class Median {

    public static float median(int[] array) {
        int arrayLength = array.length;
        Arrays.sort(array);
        if (arrayLength % 2 == 1) {
            return array[arrayLength / 2];
        } else {
            return (float) (array[arrayLength / 2] + array[arrayLength / 2 - 1]) / 2;
        }
    }

    public static double median(double[] array) {
        int arrayLength = array.length;
        Arrays.sort(array);
        if (arrayLength % 2 == 1) {
            return array[arrayLength / 2];
        } else {
            return (array[arrayLength / 2] + array[arrayLength / 2 - 1]) / 2;
        }
    }
}
